<?php 
session_start();

require_once("../conn/conexao.php");


if(!empty($_SESSION['43f6892b19a725cbb5c4048f88337c79'])){
	$usuario_id = $_SESSION['43f6892b19a725cbb5c4048f88337c79'];
}else{
	header('Location: login.php');
}

$data1 = $_GET['ini'];
$data2 = $_GET['fim'];


echo $sql = "SELECT 
			c.id,
			c.id_fornecedor,
			f.nome,
			c.valor,
			c.vencimento,
			c.descricao,
			c.status
		FROM 
			contas_pagar as c
			left join fornecedor as f ON
			c.id_fornecedor = f.id
		WHERE 
			c.vencimento between '$data1' and '$data2'";

// echo $sql = "SELECT 
// c.id,
// f.nome,
// c.valor,
// c.vencimento,
// c.descricao,
// c.status
// FROM 
// `contas_pagar` as c
// inner join fornecedor as f ON
// c.id_fornecedor = f.id
// where 
// ";

$res = mysqli_query($conn,$sql);

?>
    <thead>
         <tr>
           <th>Vencimento</th>
           <th>Valor</th>
           <th>Fornecedor</th>
           <th>Descricao</th>
           <th width="10%">Pagar</th>
         </tr>
       </thead>
       <tfoot>
         <tr>
           <th>Vencimento</th>
           <th>Valor</th>
           <th>Fornecedor</th>
           <th>Descricao</th>
           <th width="10%">Pagar</th>
         </tr>
       </tfoot>
      <tbody>
      <?php
      while($row = mysqli_fetch_array($res)) { 
        if ($row['id_fornecedor']==0) {
          $saida = "Pagamento combustível";
        }else if ($row['id_fornecedor'] == -1) {
          $saida = "Pagamento pedágio";
        }
        $responsavel = $row['nome'] == null ? $saida : $row['nome'];
      
    	?>
	     <tr > 
         <td><?php echo date('d/m/Y',strtotime($row['vencimento']));?></td>
         <td><?php echo "R$ ".number_format($row['valor'], 2, '.', '');?></td>
         <td><?php echo $responsavel;?></td>
         <td><?php echo $row['descricao'];?></td>
	     	<?php if($row['status'] == 0){ ?>
	     	<td><center><button class="btn btn-danger btn-circle" onclick="pagar(<?php echo $row['id'];?>)" ><i class="fas fa-download" ></i></button></center></td>
	     	<?php }else{ ?>
	     	<td>Pago</td>
	     	<?php }?>
	     </tr>
     <?php }?>	
    </tbody>