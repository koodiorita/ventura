<?php
session_start();
//Incluir a conexão com o BD
include_once("../conn/conexao.php");

$ids = $_POST['ids_fretes'];
$count = $_POST['count_frete'];
$vencimento = $_POST['data_vencimento'];
$valor = 0;

$string_ids = substr($ids , 0, -1); // 1;3

$ids = explode(';' ,$string_ids);


foreach($ids as $i =>$key) {

    $sql = "update frete set status = 1 where id = $key";
    $res = mysqli_query($conn,$sql);

    $sql = "select valor from frete where id = $key";
    $res = mysqli_query($conn,$sql);

    while($row = mysqli_fetch_array($res)){
        $valor += $row['valor'];
    }
    $id_frete = $key;
}
// COMO TODOS OS SELECIONADOS VÃO SER DO MESMO CLIENTE, PEGO 1 FRETE E VERIFICO QUAL O CLIENTE DA VIAGEM (CLIENTE VAI SER IGUAL P TODOS)
$sql = "SELECT 
            cli.razao_social,
            cli.responsavel
        FROM frete as f
        INNER JOIN viagem as v on
            f.id_viagem = v.id
        INNER JOIN cliente as cli on
            v.id_cliente = cli.id
        WHERE f.id = $id_frete";
$res = mysqli_query($conn,$sql);
while($row = mysqli_fetch_array($res)){
    $responsavel = $row['razao_social']." - ".$row['responsavel'];
}

// Gerando contas a pagar

$sql = "INSERT INTO contas_receber(responsavel,parcela,valor_parcela,vencimento,tipo,tipo_pagamento)
        VALUES('$responsavel','1/1',$valor,'$vencimento','1x no Boleto','BOLETO')";
$res = mysqli_query($conn,$sql);

if($res){
    $_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Conta cadastrada com sucesso</div>";
    header("Location: ../index.php#relatorio_frete");	
}else{
    $_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-danger'>Erro ao cadastrar Conta</div>";
    header("Location: ../index.php#relatorio_frete");	
}