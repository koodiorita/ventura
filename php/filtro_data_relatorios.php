<?php 
session_start();

require_once("../conn/conexao.php");


$data1 = $_GET['ini'];
$data2 = $_GET['fim'];

if(!empty($_SESSION['43f6892b19a725cbb5c4048f88337c79'])){
	$usuario_id = $_SESSION['43f6892b19a725cbb5c4048f88337c79'];
}else{
	header('Location: login.php');
}

$tipo_rel = $_GET['tipo'];

if ($tipo_rel == "combustivel") {
	$x = "combustivel";
}else if($tipo_rel == "pedagio"){
	$x = "pedagio";
}else if($tipo_rel == "frete"){
	$x = "frete";
}

echo $sql  = "SELECT
            x.id,
            x.valor,
            d.razao_social,
            cli.razao_social as nome_cliente,
			x.data_cad,
            x.status
        FROM $x as x
            INNER JOIN viagem as v on
                x.id_viagem = v.id
            INNER JOIN cliente as cli on
                cli.id = v.id_cliente
            INNER JOIN destino as d on
                d.id = v.id_destino
		WHERE x.data_cad between '$data1' and '$data2'
		";
		
$res = mysqli_query($conn,$sql);

$count = mysqli_num_rows($res);

?>   
    <thead>
      <tr>
        <th width="5%">Check</th>
        <th>Viagem</th>
        <th>Responsavel</th>
        <th>Valor</th>
        <th>Status</th>
      </tr>
    </thead>
    <tbody>
	<?php
	$total = 0;
	while($row = mysqli_fetch_array($res)) { 
    	$total += $row['valor'];
    	if ($row['status']==1) {
    	    $status = "Conta gerada";
    	}else if($row['status']==0){
    	    $status = "Em aberto";
    	}

		?>
	<tr>
        <?php if($row['status']==0){ ?>
		<td>
            <center>
                <input type="checkbox" class="form-control" style="width:25px;" id="<?=$x?>_<?=$row['id']?>" value="<?=$row['id']?>">
            </center>
        </td>
        <?php } else if ($row['status']==1){ ?>
            <td>
                <center>Gerada</center>
            </td>
        <?php } ?>
        <td><?= $row['razao_social'] ?></td>
        <td><?= $row['nome_cliente'] ?></td>
        <td><?= "R$ ".number_format($row['valor'],2,'.','') ?></td>
		<td><?=$status?></td>
	</tr>
		<?php }?>	
    </tbody>
    <tfoot>
      <tr>
        <th width="5%">Check</th>
        <th>Viagem</th>
        <th>Responsavel</th>
        <th>Valor <?= "R$ ".number_format($total,2,'.','') ?></th>
        <th>Status</th>
      </tr>
    </tfoot>