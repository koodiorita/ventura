<?php

session_start();

require_once("../conn/conexao.php");


if(!empty($_SESSION['43f6892b19a725cbb5c4048f88337c79'])){
	$usuario_id = $_SESSION['43f6892b19a725cbb5c4048f88337c79'];
}else{
	header('Location: login.php');
}

$filter_caminhao  = $_GET['filter_caminhao'];
$filter_cliente   = $_GET['filter_cliente'];
$filter_motorista = $_GET['filter_motorista'];
$filter_data_ini  = $_GET['filter_data_ini'];
$filter_data_fim  = $_GET['filter_data_fim'];

$where = "";
$count = 0;
if(!empty($filter_caminhao) || !empty($filter_cliente) || !empty($filter_motorista)){
    $count = 1;
    $where = "where ca.id like '%$filter_caminhao%' and c.id like '%$filter_cliente%' and f.id like '%$filter_motorista%'";
}

if($count == 1 && !empty($filter_data_ini) && !empty($filter_data_fim)){
    $where .= " and v.data_cad >= '$filter_data_ini 00:00:00' and v.data_cad <= '$filter_data_fim 23:59:59' ";
}
if($count == 0 && !empty($filter_data_ini) && !empty($filter_data_fim)){
    $where = " where v.data_cad >= '$filter_data_ini 00:00:00' and v.data_cad <= '$filter_data_fim 23:59:59' ";
}

$sql = "SELECT 
            v.id,
            c.razao_social,
            ca.marca,
            ca.modelo,
            f.nome,
            d.cidade,
            v.valor_frete,
            v.data_cad,
            v.data_fim,
            ca.placa
        FROM 
            viagem as v 
            inner join cliente as c ON
            v.id_cliente = c.id
            inner join caminhao as ca ON
            v.id_caminhao = ca.id
            inner join funcionario as f ON
            v.id_motorista = f.id
            inner join destino as d ON
            v.id_destino = d.id
        $where";


$res = mysqli_query($conn,$sql);

?>

<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    <thead>
    <tr>
        <th>Data Inicio</th>
        <th>Data Fim</th>
        <th>Cliente</th>
        <th>Destino</th>
        <th>Marca / Modelo / Placa</th>
        <th>Motorista</th>
        <th>View</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th>Data Inicio</th>
        <th>Data Fim</th>
        <th>Cliente</th>
        <th>Destino</th>
        <th>Marca / Modelo / Placa</th>
        <th>Motorista</th>
        <th>View</th>
    </tr>
    </tfoot>
    <tbody>
        <?php
        while($row = mysqli_fetch_array($res)) { 
            ?>
            <tr>
                <td><?php echo date('d/m/Y H:i:s',strtotime($row['data_cad']));?></td>
                <td><?php echo date('d/m/Y H:i:s',strtotime($row['data_fim']));?></td>
                <td><?php echo $row['razao_social'];?></td>
                <td><?php echo $row['cidade'];?></td>
                <td><?php echo $row['marca']." / ".$row['modelo'] . " / ".$row['placa'];?></td>
                <td><?php echo $row['nome'];?></td>
                <td><center><button class="btn btn-primary btn-circle" onclick="visualizaViagem(<?php echo $row['id'];?>)" ><i class="fas fa-edit" ></i></button></center></td>
            </tr>
        <?php }?>	
    </tbody>
</table>