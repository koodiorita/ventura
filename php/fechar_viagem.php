<?php
date_default_timezone_set("America/Sao_Paulo");

session_start();

if(!empty($_SESSION['43f6892b19a725cbb5c4048f88337c79'])){
	$usuario_id = $_SESSION['43f6892b19a725cbb5c4048f88337c79'];
}else{
	header('Location: login.php');
}

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

function getValorCaixa(){
    global $conn;

    $sql = "select valor from caixa";

    $res= mysqli_query($conn,$sql);

    while($row = mysqli_fetch_array($res)){
        $caixa = $row['valor'];
    }


    return $caixa;
}
function getUser($id_user){
    global $conn;
    $sql = "select * from user where id = $id_user";
    $res = mysqli_query($conn,$sql);
    while($row = mysqli_fetch_array($res)){
        $user = $row['usuario'];
    }

    return $user;
}

function getDestino($id_viagem){
    global $conn;
    $sql = "select d.cidade from viagem as v inner join destino as d on v.id_destino = d.id where v.id = $id_viagem";
    $res = mysqli_query($conn,$sql);
    while($row = mysqli_fetch_array($res)){
        $cidade = $row['cidade'];
    }

    return $cidade;
}
function getCliente($id_viagem){
    global $conn;

    $sql = "select c.razao_social from viagem as v inner join cliente as c on v.id_cliente = c.id where v.id = $id_viagem";

    $res = mysqli_query($conn,$sql);

    while($row = mysqli_fetch_array($res)){
        $cliente = $row['razao_social'];
    }

    return $cliente;
}
//Receber os dados do formulário
$id_viagem_enc			= $_POST['id_viagem_enc'];
$valor_descarga_enc		= $_POST['valor_descarga_enc'];
$valor_pedagio_enc		= $_POST['valor_pedagio_enc'];
$valor_combustivel_enc	= $_POST['valor_combustivel_enc'];
$qtd_combustivel_enc	= $_POST['qtd_combustivel_enc'];
$valor_frete_enc	    = $_POST['valor_frete_enc'];
$cte_enc       		    = $_POST['cte_enc'];
$arquivo_cte_enc    	= $_FILES['arquivo_cte_enc'];
$km_enc          		= $_POST['km_enc'];
$data_chegada_enc       = $_POST['data_chegada_enc'];
$num_nf_enc          	= $_POST['num_nf_enc'];
$arquivo_canhoto_enc    = $_FILES['arquivo_canhoto_enc'];
$obs_viagem_enc         = $_POST['obs_viagem_enc'];

$data_fim = date('Y-m-d H:i:s');

//Validação dos campos
if(empty($_POST['id_viagem_enc']) || empty($_POST['valor_descarga_enc']) || empty($_POST['valor_pedagio_enc']) 
|| empty($_POST['valor_combustivel_enc']) || empty($_POST['valor_frete_enc']) || empty($_POST['cte_enc'])
|| empty($_POST['km_enc']) || empty($_POST['data_chegada_enc']) 
|| empty($_POST['num_nf_enc'])){
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Preencha os campos corretamente</div>";
	header("Location: ../index.php#viagem"); 
}else{
    //Salvar no BD

    if($arquivo_cte_enc != null){
        $arquivo_cte_enc = file_get_contents($_FILES['arquivo_cte_enc']['tmp_name']);
        $arquivo_cte_enc = base64_encode($arquivo_cte_enc);
    }else{
        $arquivo_cte_enc = NULL;
    }
    if($arquivo_canhoto_enc != null){
        $arquivo_canhoto_enc = file_get_contents($_FILES['arquivo_canhoto_enc']['tmp_name']);
        $arquivo_canhoto_enc = base64_encode($arquivo_canhoto_enc);
    }else{
        $arquivo_canhoto_enc = NULL;
    }


    // UPDATE DE INFORMAÇÕES E BAIXA DA VIAGEM
    $result_data = "update viagem set valor_descarga = $valor_descarga_enc, valor_pedagio = $valor_pedagio_enc, 
            valor_combustivel = $valor_combustivel_enc, qtd_combustivel = $qtd_combustivel_enc, valor_frete = $valor_frete_enc, cte = $cte_enc,
            arquivo_cte = '$arquivo_cte_enc', km = $km_enc, data_chegada = '$data_chegada_enc', numero_nota = $num_nf_enc,
            arquivo_canhoto  = '$arquivo_canhoto_enc', obs = '$obs_viagem_enc', status = 1 , data_fim = '$data_fim' where id = $id_viagem_enc";
    $resultado_data = mysqli_query($conn, $result_data);


	//Verificar se salvou no banco de dados através do "mysqli_insert_id" que verifica se existe o ID do ultimo dado inserido
	if($resultado_data){

    
        // VALOR DE DESCARGA DEBITADO DO CAIXA E CRIANDO LOG
        $valor_caixa = getValorCaixa();
        $valor_atualizado = $valor_descarga_enc - $valor_caixa;
        $sql = "update caixa set valor = $valor_atualizado";
        mysqli_query($conn,$sql);
        $texto_log = "FECHAMENTO DE VIAGEM <br>Destino: ".getDestino($id_viagem_enc)." <br>Valor Descarga debitado: ".$valor_descarga_enc."<br>Aberto por: ".getUser($usuario_id) . " às " .date('d/m/Y H:i:s');
        $sql = "insert into log (log) value ('$texto_log')";
        mysqli_query($conn,$sql);

        // insert do frete na tabela frete

        $sql = "INSERT INTO frete(id_viagem,valor) 
                VALUES ($id_viagem_enc,$valor_frete_enc)";
        $res = mysqli_query($conn,$sql);


        // INSERE CONTAS A RECEBER
        
        // if(date('d') < 20){
        //     $vencimento = date('Y-m')."-25";
        // }else{
        //     $mes = date('m') + 1;

        //     $data = date('Y-').$mes."-25";

        //     $vencimento = date('Y-m-d',strtotime($data));
        // }

        // $cliente = getCliente($id_viagem_enc);
        // $sql = "insert into 
        //                 contas_receber (id_viagem,responsavel,parcela,valor_parcela,vencimento,tipo,tipo_pagamento,status)
        //                         values ($id_viagem_enc,'$cliente','1/1',$valor_frete_enc,'$vencimento','FRETE','',0)";
        // $res = mysqli_query($conn,$sql);



        // quando fechar viagem, insert do combustivel na tabela combustivel e pedagio na tabela pedagio

        $sql = "INSERT INTO combustivel(id_viagem,valor,quantidade) 
                VALUES ($id_viagem_enc,$valor_combustivel_enc,$qtd_combustivel_enc)";
        $res = mysqli_query($conn,$sql);

        $sql = "INSERT INTO pedagio(id_viagem,valor) 
                VALUES ($id_viagem_enc,$valor_pedagio_enc)";
        $res = mysqli_query($conn,$sql);


        // INSERE CONTAS A PAGAR 
        // if(date('d') < 20){
        //     $vencimento = date('Y-m')."-25";
        // }else{
        //     $mes = date('m') + 1;

        //     $data = date('Y-').$mes."-25";

        //     $vencimento = date('Y-m-d',strtotime($data));
        // }

        // $sql = "insert into 
        //                 contas_pagar (valor,id_viagem,vencimento,descricao)
        //                         values ($valor_pedagio_enc,$id_viagem_enc,'$vencimento','PEDAGIO')";
        // $res = mysqli_query($conn,$sql);

    
        // if(date('d') < 20){
        //     $vencimento = date('Y-m')."-25";
        // }else{
        //     $mes = date('m') + 1;

        //     $data = date('Y-').$mes."-25";

        //     $vencimento = date('Y-m-d',strtotime($data));
        // }

        // $sql = "insert into 
        //                 contas_pagar (valor,id_viagem,vencimento,descricao,status)
        //                         values ($valor_combustivel_enc,$id_viagem_enc,'$vencimento','COMBUSTIVEL',0)";
        // $res = mysqli_query($conn,$sql);

		$_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Viagem Encerrada com sucesso</div>";
		header("Location: ../index.php#viagem");		
	}else{
		$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-danger'>Erro ao encerrar viagem</div>";
	    header("Location: ../index.php#viagem");
	}
}


mysqli_close($conn);


?>