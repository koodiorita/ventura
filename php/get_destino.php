<?php

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

//Receber os dados do formulário

$id_destino = $_GET['id_destino'];

$sql = "select * from destino where id = $id_destino";

$res = mysqli_query($conn,$sql);
$data = array();
while($row = mysqli_fetch_array($res)){
	array_push($data,array('cep' => $row['cep']));
	array_push($data,array('razao_social' => $row['razao_social']));
	array_push($data,array('cnpj' => $row['cnpj']));
	array_push($data,array('taxa' => $row['taxa_descarga']));
	array_push($data,array('cidade' => $row['cidade']));
	array_push($data,array('endereco' => $row['endereco']));
	array_push($data,array('bairro' => $row['bairro']));
	array_push($data,array('numero' => $row['numero']));
	array_push($data,array('uf' => $row['uf']));
	array_push($data,array('telefone' => $row['telefone']));
}
mysqli_close($conn);
$json = json_encode($data);
echo $json;
?>