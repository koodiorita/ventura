<?php

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

//Receber os dados do formulário
$id = $_GET['id_caminhao'];
$sql = "select * from caminhao where id = $id";

$res = mysqli_query($conn,$sql);
$data = array();
while($row = mysqli_fetch_array($res)){
    array_push($data,array('renavam' => $row['renavam']));
    array_push($data,array('vencimento_doc' => $row['vencimento_doc']));
    array_push($data,array('placa' => $row['placa']));
    array_push($data,array('chassi' => $row['chassi']));
    array_push($data,array('marca' => $row['marca']));
    array_push($data,array('modelo' => $row['modelo']));
    array_push($data,array('ano_fab' => $row['ano_fab']));
    array_push($data,array('ano_mod' => $row['ano_mod']));
    array_push($data,array('cor' => $row['cor']));
    array_push($data,array('km' => $row['km']));
    array_push($data,array('placa_carreta' => $row['placa_carreta']));
    array_push($data,array('chassi_carreta' => $row['chassi_carreta']));
    array_push($data,array('modelo_carreta' => $row['modelo_carreta']));
}
mysqli_close($conn);
$json = json_encode($data);
echo $json;
?>