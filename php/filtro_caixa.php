<?php
session_start();

include_once("../conn/conexao.php");

$data1 = $_GET['ini'];
$data2 = $_GET['fim'];
$tipo = $_GET['tipo'];

$where = "";
if (!empty($data1)) {
    $where = "where data_cad between '$data1 00:00:00' AND '$data2 23:59:59' ";
    if (!empty($tipo)) {
        $where .= " AND log like '%$tipo%' ";
    }
} else {
    if (!empty($tipo)) {
        $where = "WHERE log like '%$tipo%' ";
    }
}

$sql = "select * from log $where order by data_cad desc ";
$res = mysqli_query($conn, $sql);
?>

<table class="table table-bordered" id="dataTableHistorico">
    <thead>
        <tr>
            <th width="10%">ID</th>
            <th>LOG</th>
            <th width="10%">Data</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th width="10%">ID</th>
            <th>LOG</th>
            <th width="10%">Data</th>
        </tr>
    </tfoot>
    <tbody>
        <?php

        while ($row = mysqli_fetch_array($res)) {

        ?>
            <tr>
                <td><?php echo $row['id']; ?></td>
                <td><?php echo $row['log']; ?></td>
                <td><?php echo date('d/m/Y H:i:s', strtotime($row['data_cad'])); ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>