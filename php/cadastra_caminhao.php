<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

if(!empty($_SESSION['43f6892b19a725cbb5c4048f88337c79'])){
    $usuario_id = $_SESSION['43f6892b19a725cbb5c4048f88337c79'];
  }else{
      header('Location: login.php');
  }
  
$renavam  = $_POST['renavam'];
$vencimento = $_POST['venc_doc'];
$placa    = $_POST['placa'];
$chassi   = $_POST['chassi'];
$marca    = $_POST['marca'];
$modelo   = $_POST['modelo'];
$ano_fab  = $_POST['ano-fab'];
$ano_mod  = $_POST['ano-mod'];
$cor      = $_POST['cor'];
$km       = $_POST['km'] ;
$carreta  = $_POST['radio_carreta'];
$placa_carreta  = $_POST['placa_carreta'] == null ? 0 : $_POST['placa_carreta'];
$chassi_carreta  = $_POST['chassi_carreta'] == null ? 0 : $_POST['chassi_carreta'];
$modelo_carreta  = $_POST['modelo_carreta'] == null ? 0 : $_POST['modelo_carreta'];

if(empty($_POST['renavam']) || empty($_POST['placa']) || empty($_POST['chassi']) || empty($_POST['marca']) || empty($_POST['modelo']) || empty($_POST['ano-fab']) || empty($_POST['ano-mod']) || empty($_POST['cor'])){
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Preencha os campos corretamente</div>";
	header("Location: ../index.php#caminhao"); 
}else{
    $sql = "insert into caminhao (renavam,vencimento_doc,placa,chassi,marca,modelo,ano_fab,ano_mod,cor,km,carreta,placa_carreta,chassi_carreta,modelo_carreta) 
    values('$renavam','$vencimento','$placa','$chassi','$marca','$modelo','$ano_fab','$ano_mod','$cor','$km',$carreta,'$placa_carreta','$chassi_carreta','$modelo_carreta')";
    $res = mysqli_query($conn,$sql);

    if($res){
        $_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Caminhão cadastrada com sucesso</div>";
		header("Location: ../index.php#caminhao");	
    }else{
        $_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-danger'>Erro ao cadastrar caminhão</div>";
		header("Location: ../index.php#caminhao");	
    }
}
?>