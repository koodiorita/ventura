<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

function getValorCaixa(){
    global $conn;

    $sql = "select valor from caixa";

    $res= mysqli_query($conn,$sql);

    while($row = mysqli_fetch_array($res)){
        $caixa = $row['valor'];
    }


    return $caixa;
}
function getUser($id_user){
    global $conn;
    $sql = "select * from user where id = $id_user";
    $res = mysqli_query($conn,$sql);
    while($row = mysqli_fetch_array($res)){
        $user = $row['usuario'];
    }

    return $user;
}

if(!empty($_SESSION['user_id'])){
	$usuario_id = $_SESSION['user_id'];
}else{
	header('Location: login.php');
}

//Receber os dados do formulário
$valor		    = str_replace(",",".",$_POST['valor_retirada']);
$descricao	    = $_POST['descricao_retirada'];

$valor_caixa = getValorCaixa();
$valor_caixa -= $valor;



$texto_log = "RETIRADA DE DINHEIRO <br>Valor: R$ $valor <br>Motivo: $descricao<br>Retirada feito por: ".getUser($usuario_id);

$sql = "insert into retirada_dinheiro (id_user,valor,descricao) value ($usuario_id,$valor,'$descricao')";
mysqli_query($conn,$sql);
$sql = "insert into log (log) value ('$texto_log')";
mysqli_query($conn,$sql);
$sql = "update caixa set valor = $valor_caixa";
mysqli_query($conn,$sql);

$_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Retirada Efetuada com Sucesso</div>";
header("Location: ../index.php#dashboard");
