<?php

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

//Receber os dados do formulário
$id = $_GET['id'];

$sql = "select 
            c.modelo,
            c.marca,
            cli.razao_social as cliente,
            f.nome as motorista,
            d.cidade,
            v.oc,
            v.vale,
            v.tipo_carga,
            v.obs,
            v.id_caminhao,
            v.id_cliente,
            v.id_motorista,
            v.id_destino
        from 
            viagem as v
            inner join caminhao as c on
            v.id_caminhao = c.id
            inner join cliente as cli on
            v.id_cliente = cli.id
            inner join funcionario as f on
            v.id_motorista = f.id
            inner join destino as d on
            v.id_destino = d.id
        where 
            v.id = $id";

$res = mysqli_query($conn,$sql);
$data = array();
while($row = mysqli_fetch_array($res)){ 
    
    
}
    
?>

<div class="modal fade" id="FecharViagem" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Fechar Viagem</h5>
				  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">×</span>
				  </button>
				</div>
				<div class="modal-body">
					<form action="php/fechar_viagem.php" method="POST" enctype="multipart/form-data" >
            <input type="hidden" id="id_viagem_enc" name="id_viagem_enc">
            <div class="form-row">
              <div class="col">Caminhão</div>
              <div class="col">Cliente</div>
            </div>

            <div class="form-row">
                <div class="col">
                  <input class="form-control" id="caminhao_enc" readonly />
                </div>
                <div class="col">
                  <input class="form-control" id="cliente_enc" readonly />
                </div>
            </div><br>

            <div class="form-row">
              <div class="col">Motorista</div>
              <div class="col">Destino</div>
            </div>

            <div class="form-row">
              <div class="col">
                 <input class="form-control" id="motorista_enc" readonly />
              </div>
              <div class="col">
                 <input class="form-control" id="destino_enc" readonly />
              </div>
            </div><br>

            <div class="form-row">
              <div class="col">OC</div>
              <div class="col">Vale</div>
            </div>
            <div class="form-row">
              <div class="col">
                <input type="number" class="form-control" id="oc_enc" name="oc_enc" placeholder="Ordem de Carregamento" readonly /><br>
              </div>
              <div class="col">
                <input type="number" step="0.01" class="form-control" id="vale_enc" name="vale_enc" placeholder="Vale" readonly /><br>
              </div>
            </div></br>
            
            <div class="form-row">
              <div class="col">Tipo de Carga</div>
              <div class="col"></div>
            </div>

            <div class="form-row">
              <div class="col">
                <input id="tipo_carga_enc" class="form-control"  readonly/>
              </div>
              <div class="col">
                
              </div>
            </div></br>
            
            <div class="form-row">
              <div class="col">Valor da Descarga</div>
              <div class="col">Valor do Pedagio</div>
              <div class="col">Valor do Combustivel</div>
              <div class="col">Valor do Frete</div>
            </div>

            <div class="form-row">
              <div class="col">
                <input id="valor_descarga_enc" name="valor_descarga_enc" class="form-control"  />
              </div>
              <div class="col">
                <input id="valor_pedagio_enc" name="valor_pedagio_enc" class="form-control"  />
              </div>
              <div class="col">
                <input id="valor_combustivel_enc" name="valor_combustivel_enc" class="form-control"  />
              </div>
              <div class="col">
                <input id="valor_frete_enc" name="valor_frete_enc" class="form-control"  />
              </div>
            </div></br>
            
            <div class="form-row">
              <div class="col">Nº CTE</div>
              <div class="col">Anexo CTE</div>
            </div>

            <div class="form-row">
              <div class="col">
                <input type="number" id="cte_enc" name="cte_enc" class="form-control"  />
              </div>
              <div class="col">
                <input type="file" id="arquivo_cte_enc" name="arquivo_cte_enc" class="form-control"  />
              </div>
            </div></br>

            <div class="form-row">
              <div class="col">Km Veiculo</div>
              <div class="col">Data Chegada</div>
            </div>

            <div class="form-row">
              <div class="col">
                <input type="number" step="0.01" id="km_enc" name="km_enc" class="form-control"  />
              </div>
              <div class="col">
                <input type="date" id="data_chegada_enc" name="data_chegada_enc" class="form-control"  />
              </div>
            </div></br>

            <div class="form-row">
              <div class="col">Nº NF</div>
              <div class="col">Anexo Canhoto</div>
            </div>

            <div class="form-row">
              <div class="col">
                <input type="number" id="num_nf_enc" name="num_nf_enc" class="form-control"  />
              </div>
              <div class="col">
                <input type="file" id="arquivo_canhoto_enc" name="arquivo_canhoto_enc" class="form-control"  />
              </div>
            </div></br>

            <textarea name="obs_viagem_enc" id="obs_viagem_enc" cols="15" rows="5" placeholder="Observação" class="form-control" ></textarea><br>
            
						<button class="btn btn-success" type="submit" style="float: right">Encerrar Viagem</button>
						<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
					</form>
				</div>
				  </div>
			</div>
		</div>