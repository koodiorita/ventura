<?php 
session_start();

require_once("../conn/conexao.php");


if(!empty($_SESSION['43f6892b19a725cbb5c4048f88337c79'])){
	$usuario_id = $_SESSION['43f6892b19a725cbb5c4048f88337c79'];
}else{
	header('Location: login.php');
}

$id = $_GET['id'];
$sql = "SELECT 
			c.id,
			c.parcela,
			c.valor_parcela,
			c.vencimento,
			c.tipo,
			c.tipo_pagamento,
			cli.responsavel as responsavel_cli,
			cli2.responsavel as responsavel_cli2,
			sem.id_servico,
			sem.valor_hora,
			sem.qtd_hora,
			c.status
		FROM 
			`contas_receber` as c
			left join contrato as con ON
			c.id_contrato = con.id
			left join sem_contrato as sem ON
			c.id_sem_contrato = sem.id
			left join cliente as cli ON
			con.id_cliente = cli.id 
			left join cliente as cli2 ON
			sem.id_cliente = cli2.id 
			where 
			 
                (cli.id = $id or cli2.id = $id)

            order by c.vencimento asc
		";

$res = mysqli_query($conn,$sql);

?>

<div class="card-body">
              <div class="table-responsive">
                    <div class="form-row">
                        <div class="col-3">
                            <input type="date" id="filtro-data1" class="form-control" />
                        </div>
                        <span style="align-self: center;">até</span>
                        <div class="col-3">
                            <input type="date" id="filtro-data2" class="form-control" />
                        </div>
                        <div class="col-1">
                            <button  style="float: right;margin-left: 10px" class=" btn btn-success" onclick="buscar(<?php echo $id;?>)" >Buscar</button>
                        </div>
                    </div><br>
                <table class="table table-bordered" id="dataTableFinanceiro" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Vencimento</th>
                      <th>Parcela</th>
                      <th>Valor</th>
                      <th>Pagamento / Tipo</th>
                      <th>Responsavel</th>
                      <th width="10%">Receber</th>
                    </tr>
                  </thead>
                  
                  <tbody>
						<?php
							$valor_total = 0;
						while($row = mysqli_fetch_array($res)) { 
                            $responsavel = $row['responsavel_cli'];
							
							if(strlen($responsavel) < 2){
								$responsavel = $row['responsavel_cli2'];
							}
							
							$valor = $row['valor_parcela'];
							if($row['id_servico'] == 99){
								$valor = $row['qtd_hora'] * $row['valor_hora'];
							}
							$valor_total += $valor;
							?>
							<tr>
								<td><?php echo date('d/m/Y',strtotime($row['vencimento']));?></td>
                                <td><?php echo $row['parcela'];?></td>
                                <td><?php echo "R$ ".number_format($valor, 2, '.', '');?></td>
                                <td><?php echo $row['tipo']." / ".$row['tipo_pagamento'];?></td>
                                <td><?php echo $responsavel;?></td>
								<?php if($row['status'] == 0) { ?>
								<td><center><button class="btn btn-primary btn-circle" onclick="receberCliente(<?php echo $row['id'];?>)" ><i class="fas fa-download" ></i></button></center></td>
								<?php }else{?>
								<td>Recebido</td>
								<?php }?>
							</tr>
						<?php }?>	
                  </tbody>
				  <tfoot>
                    <tr>
                      <th>Vencimento</th>
                      <th>Parcela</th>
                      <th><?php echo "R$ ".number_format($valor_total, 2, '.', '');?></th>
                      <th>Pagamento / Tipo</th>
                      <th>Responsavel</th>
                      <th width="10%">Receber</th>
                    </tr>
                  </tfoot>
                </table>
				
              </div>
            </div>

            <script>
                function receberCliente(id_receber){
                  var resp = confirm("Deseja receber essa conta ?");
  
                  if(resp == true){
                    $.get( "php/receber_conta.php?id_receber="+id_receber, function( data ) {
                        location.reload();
                    });
                  }
                    
			    }    
                function buscar(id){
                    var data1 = $("#filtro-data1").val();
                    var data2 = $("#filtro-data2").val();

                    

                    $.get( "php/filtro_data.php?ini="+data1+"&fim="+data2+"&id_cliente="+id, function( data ) {
                        $("#dataTableFinanceiro").html(data);
                    });
                }
            </script>