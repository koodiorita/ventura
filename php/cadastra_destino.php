<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

//Receber os dados do formulário
$razao			= $_POST['razao_des'];
$cnpj			= $_POST['cnpj_des'] == null ? 'NULL' : $_POST['cnpj_des'];
$cep			= $_POST['cep_des'];
$endereco		= $_POST['endereco_des'];
$numero			= $_POST['numero_des'];
$bairro			= $_POST['bairro_des'];
$cidade			= $_POST['cidade_des'];
$uf 			= $_POST['uf_des'];
$telefone		= $_POST['telefone_des'];
$taxa   		= $_POST['taxa_des'] == null ? 0 : $_POST['taxa_des'];


//Validação dos campos
if(empty($_POST['razao_des']) || empty($_POST['endereco_des']) || empty($_POST['bairro_des'])  || empty($_POST['cidade_des']) || empty($_POST['numero_des']) || empty($_POST['cep_des']) ){
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Preencha os campos corretamente</div>";
	header("Location: ../index.php#destino"); 
}else{
	//Salvar no BD
	$result_data = "INSERT INTO destino(cep,razao_social,cnpj,cidade,endereco,bairro,numero,uf,telefone,taxa_descarga) 
    value('$cep','$razao','$cnpj','$cidade','$endereco','$bairro','$numero','$uf','$telefone',$taxa)";
	$resultado_data = mysqli_query($conn, $result_data);

	//Verificar se salvou no banco de dados através do "mysqli_insert_id" que verifica se existe o ID do ultimo dado inserido
	if(mysqli_insert_id($conn)){
		$_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Destino cadastrado com sucesso</div>";
		header("Location: ../index.php#destino");		
	}else{
		$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-danger'>Erro ao cadastrar destino</div>";
		header("Location: ../index.php#destino");
	}
	
}


mysqli_close($conn);


?>