<?php 
session_start();

require_once("../conn/conexao.php");


$data1 = $_GET['ini'];
$data2 = $_GET['fim'];

if(!empty($_SESSION['43f6892b19a725cbb5c4048f88337c79'])){
	$usuario_id = $_SESSION['43f6892b19a725cbb5c4048f88337c79'];
}else{
	header('Location: login.php');
}


$sql = "SELECT 
			id,
			vencimento,
			valor_parcela,
			tipo,
			responsavel,
			status
		FROM 
			contas_receber
		where  
            vencimento between '$data1' and '$data2'
 		";

$res = mysqli_query($conn,$sql);


?>   
    <thead>
      <tr>
        <th>Vencimento</th>
        <th>Valor</th>
        <th>Pagamento / Tipo</th>
        <th>Responsavel</th>
        <th width="10%">Receber</th>
        <th width="10%">Anular</th>
      </tr>
    </thead>
    
    <tbody>
	<?php
		$valor_total = 0;
	while($row = mysqli_fetch_array($res)) { 
		$id_cliente = $row['id_cliente'];
		$valor_total += $row['valor_parcela'];
	?>
    <tr>
    	<td><?php echo date('d/m/Y',strtotime($row['vencimento']));?></td>	
        <td><?php echo "R$ ".number_format($row['valor_parcela'], 2, '.', '');?></td>
        <td><?php echo $row['tipo'];?></td>
        <td><?php echo $row['responsavel'];?></td>

        <?php if($row['status'] == 0) { ?>
            
    	<td><center><button class="btn btn-primary btn-circle" onclick="receber(<?php echo $row['id'];?>)" ><i class="fas fa-download" ></i></button></center></td>
    	<td><center><button class="btn btn-danger btn-circle" onclick="cancel(<?php echo $row['id'];?>)" ><i class="fas fa-window-close" ></i></button></center></td>
    	<?php }else if($row['status']==1){?>
			  <td>Recebido</td>
			  <td>Não é possível</td>
			<?php }else if($row['status']==2){?>
			  <td>Anulado</td>
			  <td>Já anulado</td>
			<?php } ?>
    </tr>
	<?php }?>	
    </tbody>
	  <tfoot>
        <tr>
          <th>Vencimento</th>
          <th><?php echo "R$ ".number_format($valor_total, 2, '.', '');?></th>
          <th>Pagamento / Tipo</th>
          <th>Responsavel</th>
          <th width="10%">Receber</th>
		  <th width="10%">Anular</th>
        </tr>
      </tfoot>