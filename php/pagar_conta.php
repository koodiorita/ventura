<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

function getValorCaixa(){
    global $conn;

    $sql = "select valor from caixa";

    $res= mysqli_query($conn,$sql);

    while($row = mysqli_fetch_array($res)){
        $caixa = $row['valor'];
    }


    return $caixa;
}

function getUser($id_user){
    global $conn;
    $sql = "select * from user where id = $id_user";
    $res = mysqli_query($conn,$sql);
    while($row = mysqli_fetch_array($res)){
        $user = $row['usuario'];
    }

    return $user;
}

if(!empty($_SESSION['43f6892b19a725cbb5c4048f88337c79'])){
    $usuario_id = $_SESSION['43f6892b19a725cbb5c4048f88337c79'];
  }else{
      header('Location: login.php');
  }


$id_conta  = $_POST['id_conta_pagar'];
$id_banco  = $_POST['banco'];

$comprovante  = $_FILES['arquivo'];

if($comprovante != null){
    $comprovante = file_get_contents($_FILES['arquivo']['tmp_name']);
    $comprovante = base64_encode($comprovante);
}

$sql = "select 
                c.id,
                c.valor,
                c.id_fornecedor,
                f.nome
            from 
                contas_pagar as c
                left join fornecedor as f ON
                c.id_fornecedor = f.id  
            where c.id = $id_conta
        ";




$resGeral = mysqli_query($conn,$sql);

while($row = mysqli_fetch_array($resGeral)){
    $valor = $row['valor'];
    
    $valor_caixa = getValorCaixa();
    $valor_caixa -= $valor;

    //update no status da conta
    $sql = "update contas_pagar set status = 1, comprovante = '$comprovante' where id = $id_conta";
    $res = mysqli_query($conn,$sql);

    

    if($res){
        if ($row['id_fornecedor']==0) {
            $fornecedor = "Combustivel";
        }else if($row['id_fornecedor']==-1){
            $fornecedor = "Pedágio";
        }else{
            $fornecedor = $row['nome'];
        }
        $texto_log = "CAIXA ANTIGO: ".getValorCaixa()." <br> CAIXA NOVO: $valor_caixa <br> CONTAS PAGAR VALOR: R$ $valor <br> CLIENTE/FORNECEDOR: $fornecedor <br>  user: ".getUser($usuario_id);

        $sql = "insert into log (log) value ('$texto_log')";
        mysqli_query($conn,$sql);
        $sql = "update caixa set valor = $valor_caixa";
        mysqli_query($conn,$sql);

        //update banco selecionado
        $sql = "UPDATE banco set valor = valor - $valor where id=$id_banco";
        $res = mysqli_query($conn,$sql);

        if ($res) {
            $_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Conta Paga</div>";
            header("Location: ../index.php#contas_pagar");
        }else{
            $_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-danger'>Erro ao debitar do banco</div>";	
            header("Location: ../index.php#contas_pagar");
        }
    }else{
        $_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-danger'>Erro ao pagar</div>";	
        header("Location: ../index.php#contas_pagar");
    }
    
}



	


?>