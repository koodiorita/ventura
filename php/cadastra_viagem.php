<?php
date_default_timezone_set("America/Sao_Paulo");
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

function getValorCaixa(){
    global $conn;

    $sql = "select valor from caixa";

    $res= mysqli_query($conn,$sql);

    while($row = mysqli_fetch_array($res)){
        $caixa = $row['valor'];
    }


    return $caixa;
}

function getUser($id_user){
    global $conn;
    $sql = "select * from user where id = $id_user";
    $res = mysqli_query($conn,$sql);
    while($row = mysqli_fetch_array($res)){
        $user = $row['usuario'];
    }

    return $user;
}

function getDestino($id_destino){
    global $conn;
    $sql = "select * from destino where id = $id_destino";
    $res = mysqli_query($conn,$sql);
    while($row = mysqli_fetch_array($res)){
        $cidade = $row['cidade'];
    }

    return $cidade;
}

//Receber os dados do formulário
$caminhao			= $_POST['caminhao'];
$cliente			= $_POST['cliente'];
$motorista			= $_POST['motorista'];
$destino			= $_POST['destino'];
$oc 	      		= $_POST['oc'];
$vale       		= $_POST['vale'];
$tipo_carga    		= $_POST['tipo_carga'];
$obs          		= $_POST['obs_viagem'];




//Validação dos campos
if(empty($_POST['caminhao']) || empty($_POST['cliente']) || empty($_POST['motorista']) || empty($_POST['destino']) || empty($_POST['vale']) || empty($_POST['tipo_carga'])){
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Preencha os campos corretamente</div>";
	header("Location: ../index.php#viagem"); 
}else{
	//Salvar no BD
	$result_data = "INSERT INTO viagem(id_caminhao,id_cliente,id_motorista,id_destino,oc,vale,tipo_carga,obs) 
    value($caminhao,$cliente,$motorista,$destino,'$oc',$vale,'$tipo_carga','$obs')";
	$resultado_data = mysqli_query($conn, $result_data);

	//Verificar se salvou no banco de dados através do "mysqli_insert_id" que verifica se existe o ID do ultimo dado inserido
	if(mysqli_insert_id($conn)){

		$valor_caixa = getValorCaixa();

		$valor_atualizado = $valor_caixa - $vale;

		$sql = "update caixa set valor = $valor_atualizado";
		mysqli_query($conn,$sql);
		
		$texto_log = "ABERTURA DE VIAGEM <br>Destino: ".getDestino($destino)." <br>Vale debitado: ".$vale."<br>Aberto por: ".getUser($usuario_id) . " às " .date('d/m/Y H:i:s');

		$sql = "insert into log (log) value ('$texto_log')";
		mysqli_query($conn,$sql);

		$_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Viagem cadastrada com sucesso</div>";
		header("Location: ../index.php#viagem");		
	}else{
		$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-danger'>Erro ao cadastrar viagem</div>";
	    header("Location: ../index.php#viagem");
	}
}


mysqli_close($conn);


?>