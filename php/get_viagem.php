<?php

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

//Receber os dados do formulário
$id = $_GET['id_viagem'];
$sql = "select 
            c.modelo,
            c.marca,
            cli.razao_social as cliente,
            f.nome as motorista,
            d.cidade,
            v.oc,
            v.vale,
            v.tipo_carga,
            v.id_caminhao,
            v.id_cliente,
            v.id_motorista,
            v.id_destino,
            v.valor_descarga,
            v.valor_pedagio,
            v.valor_combustivel,
            v.valor_frete,
            v.cte,
            v.km,
            DATE_FORMAT(v.data_chegada,'%Y-%m-%d') AS data_chegada,
            v.numero_nota,
            v.obs
        from 
            viagem as v
            inner join caminhao as c on
            v.id_caminhao = c.id
            inner join cliente as cli on
            v.id_cliente = cli.id
            inner join funcionario as f on
            v.id_motorista = f.id
            inner join destino as d on
            v.id_destino = d.id
        where 
            v.id = $id";

$res = mysqli_query($conn,$sql);
$data = array();
while($row = mysqli_fetch_array($res)){
    array_push($data,array('id_caminhao'  => $row['modelo']."/".$row['marca'] ));
    array_push($data,array('id_cliente'   => $row['cliente']));
    array_push($data,array('id_motorista' => $row['motorista']));
    array_push($data,array('id_destino'   => $row['cidade']));
    array_push($data,array('oc'           => $row['oc']));
    array_push($data,array('vale'         => $row['vale']));
    array_push($data,array('tipo_carga'   => $row['tipo_carga']));
    array_push($data,array('obs'          => $row['obs']));

    array_push($data,array('id_caminhao_c'  => $row['id_caminhao']));
    array_push($data,array('id_cliente_c'   => $row['id_cliente']));
    array_push($data,array('id_motorista_c' => $row['id_motorista']));
    array_push($data,array('id_destino_c'   => $row['id_destino']));

    array_push($data,array('valor_descarga_c'    => $row['valor_descarga']));
    array_push($data,array('valor_pedagio_c'     => $row['valor_pedagio']));
    array_push($data,array('valor_combustivel_c' => $row['valor_combustivel']));
    array_push($data,array('valor_frete_c'       => $row['valor_frete']));
    array_push($data,array('cte_c'               => $row['cte']));
    array_push($data,array('km_c'                => $row['km']));
    array_push($data,array('data_chegada_c'      => $row['data_chegada']));
    array_push($data,array('numero_nota_c'       => $row['numero_nota']));


}
mysqli_close($conn);
$json = json_encode($data);
echo $json;
?>