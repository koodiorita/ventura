<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

//Receber os dados do formulário
$id			        = $_POST['id_viagem_edit'];
$caminhao			= $_POST['caminhao_edit'];
$cliente			= $_POST['cliente_edit'];
$motorista			= $_POST['motorista_edit'];
$destino			= $_POST['destino_edit'];
$oc 	      		= $_POST['oc_edit'];
$vale       		= $_POST['vale_edit'];
$tipo_carga    		= $_POST['tipo_carga_edit'];
$obs          		= $_POST['obs_viagem_edit'];




//Validação dos campos
if(empty($_POST['caminhao_edit']) || empty($_POST['cliente_edit']) || empty($_POST['motorista_edit']) 
|| empty($_POST['destino_edit']) || empty($_POST['vale_edit']) || empty($_POST['tipo_carga_edit'])){
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Preencha os campos corretamente</div>";
	header("Location: ../index.php#dashboard"); 
}else{
    $result_data = "update viagem set id_caminhao = $caminhao, id_cliente = $cliente, id_motorista = $motorista, 
                        id_destino = $destino, oc = '$oc', vale = $vale, tipo_carga = '$tipo_carga', obs = '$obs' where id = $id";
    $resultado_data = mysqli_query($conn, $result_data);

    //Verificar se salvou no banco de dados através do "mysqli_insert_id" que verifica se existe o ID do ultimo dado inserido
    if($resultado_data){
        $_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Viagem Editada com sucesso</div>";
        header("Location: ../index.php#dashboard");		
    }else{
        $_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-danger'>Erro ao editar viagem</div>";
        header("Location: ../index.php#dashboard");
    }
}


mysqli_close($conn);

