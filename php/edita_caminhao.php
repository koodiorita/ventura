<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

if (!empty($_SESSION['43f6892b19a725cbb5c4048f88337c79'])) {
    $usuario_id = $_SESSION['43f6892b19a725cbb5c4048f88337c79'];
} else {
    header('Location: login.php');
}

$id       = $_POST['id_caminhao_edit'];
$renavam  = $_POST['renavam_edit'];
$vencimento = $_POST['venc_doc_edit'];
$placa    = $_POST['placa_edit'];
$chassi   = $_POST['chassi_edit'];
$marca    = $_POST['marca_edit'];
$modelo   = $_POST['modelo_edit'];
$ano_fab  = $_POST['ano-fab_edit'];
$ano_mod  = $_POST['ano-mod_edit'];
$cor      = $_POST['cor_edit'];
$km       = $_POST['km_edit'];
$radio_carreta = $_POST['radio_carreta_edit'];
if ($radio_carreta == 1) {
    $placa_carreta = $_POST['placa_carreta_edit'];
    $chassi_carreta       = $_POST['chassi_carreta_edit'];
    $modelo_carreta       = $_POST['modelo_carreta_edit'];
} else {
    $placa_carreta = "";
    $chassi_carreta = "";
    $modelo_carreta = "";
}


if (empty($_POST['renavam_edit']) || empty($_POST['venc_doc_edit']) || empty($_POST['placa_edit']) || empty($_POST['chassi_edit']) || empty($_POST['marca_edit']) || empty($_POST['modelo_edit']) || empty($_POST['ano-fab_edit']) || empty($_POST['ano-mod_edit']) || empty($_POST['cor_edit']) || empty($_POST['km_edit'])) {
    $_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Preencha os campos corretamente</div>";
    header("Location: ../index.php#caminhao");
} else {
    echo $sql = "update caminhao set renavam = '$renavam',vencimento_doc = '$vencimento', placa = '$placa', chassi = '$chassi', marca = '$marca',
    modelo = '$modelo', ano_fab = '$ano_fab' , ano_mod = '$ano_mod', cor = '$cor', km = '$km', carreta = $radio_carreta, placa_carreta = '$placa_carreta', 
    chassi_carreta = '$chassi_carreta', modelo_carreta = '$modelo_carreta' where id = $id";
    $res = mysqli_query($conn, $sql);

    if ($res) {
        $_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Caminhão editado com sucesso</div>";
        header("Location: ../index.php#caminhao");	
    } else {
        $_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-danger'>Erro ao editar caminhão</div>";
        header("Location: ../index.php#caminhao");	
    }
}
