<?php
date_default_timezone_set("America/Sao_Paulo");
session_start();

require_once("../conn/conexao.php");


if (!empty($_SESSION['43f6892b19a725cbb5c4048f88337c79'])) {
	$usuario_id = $_SESSION['43f6892b19a725cbb5c4048f88337c79'];
} else {
	header('Location: login.php');
}


$sql = "select * from log order by id desc ";

$res = mysqli_query($conn, $sql);




?>
<style>
	table.dataTable tbody th,
	table.dataTable tbody td {
		text-align-last: left;
	}

	.onoff input.toggle {
		display: none;
	}

	.onoff input.toggle+label {
		display: inline-block;
		position: relative;
		box-shadow: inset 0 0 0px 1px #d5d5d5;
		height: 20px;
		width: 40px;
		border-radius: 30px;
	}

	.onoff input.toggle+label:before {
		content: "";
		display: block;
		height: 20px;
		width: 40px;
		border-radius: 30px;
		background: rgba(19, 191, 17, 0);
		transition: 0.1s ease-in-out;
	}

	.onoff input.toggle+label:after {
		content: "";
		position: absolute;
		height: 20px;
		width: 20px;
		top: 0;
		left: 0px;
		border-radius: 30px;
		background: #fff;
		box-shadow: inset 0 0 0 1px rgba(0, 0, 0, 0.2), 0 2px 4px rgba(0, 0, 0, 0.2);
		transition: 0.1s ease-in-out;
	}

	.onoff input.toggle:checked+label:before {
		width: 40px;
		background: #13bf11;
	}

	.onoff input.toggle:checked+label:after {
		left: 20px;
		box-shadow: inset 0 0 0 1px #13bf11, 0 2px 4px rgba(0, 0, 0, 0.2);
	}

	.xx {
		float: right;
		background: #ccc;
		border-radius: 200px;
		width: 14px;
		height: 13px;
		color: white;
		text-align: center;
		font-size: 10px;
	}

	.xx:hover {
		background: #777;
		cursor: pointer
	}

	.dataTables_wrapper .dataTables_filter input {
		border-radius: 10px;
		border: 1px solid #ccc;
		outline-style: none;
	}
</style>
<div class="container-fluid">



	<!-- DataTales Example -->
	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<div class="form-row">
				<div class="col">
					<h4 class="m-0 font-weight-bold text-primary">Histórico <?php echo date('d/m/Y H:i:s'); ?>
					</h4>
				</div>
				<div class="col-2"><input type="date" id="filtro-data1" class="form-control" /></div>
				<span style="align-self: center;">até</span>
				<div class="col-2"><input type="date" id="filtro-data2" class="form-control" /></div>
				<div class="col-2">
					<select class="form-control" name="tipoPesquisa" id="tipoPesquisa">
						<option value="">Selecione o tipo</option>
						<option value="ABERTURA DE VIAGEM">Abertura de Viagem</option>
						<option value="FECHAMENTO DE VIAGEM">Fechamento de Viagem</option>
						<option value="CONTAS ANULADA">Contas Anuladas</option>
						<option value="CONTA RECEBIDA">Contas Recebidas</option>
					</select>
				</div>
				<div class="col-2"><button style="float: right;margin-left: 10px" class=" btn btn-success" onclick="buscar()">Buscar</button></div>
			</div>

		</div>
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-bordered" id="dataTableHistorico" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th width="10%">ID</th>
							<th>LOG</th>
							<th width="10%">Data</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th width="10%">ID</th>
							<th>LOG</th>
							<th width="10%">Data</th>
						</tr>
					</tfoot>
					<tbody>
						<?php

						while ($row = mysqli_fetch_array($res)) {

						?>
							<tr>
								<td><?php echo $row['id']; ?></td>
								<td><?php echo $row['log']; ?></td>
								<td><?php echo date('d/m/Y H:i:s', strtotime($row['data_cad'])); ?></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

</div>
<script>
	$(document).ready(function() {
		$('#dataTableHistorico').DataTable({});
	});

	function buscar() {
		var data1 = $("#filtro-data1").val();
		var data2 = $("#filtro-data2").val();
		var tipo = $("#tipoPesquisa").val();

		if (data1.length > 0 || tipo != "") {
			$.get("php/filtro_caixa.php?ini=" + data1 + "&fim=" + data2 + "&tipo=" + tipo, function(data) {
				$("#dataTableHistorico").html(data);
			});
		} else {
			alert('Preencha pelo menos um dos campos.');
		}
	}
</script>