	<!-- AddProd -->
    <div class="modal fade" id="AddDestino" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Cadastro de Destino</h5>
				  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				  </button>
				</div>
				<div class="modal-body">
					<form action="php/cadastra_destino.php" method="POST"  >
                        <div class="form-row">
                            <div class="col">
                                <input name="razao_des" type="text" placeholder="Nome da Empresa" class="form-control" required /><br>
                            </div>
                            <div class="col">
                                <input name="cnpj_des" id="cnpj_des" type="text" placeholder="CNPJ" class="form-control"  /><br>
                            </div>
                        </div>
                            
                        <div class="form-row">
                            <div class="col-4">
                                <input id="cep_des" name="cep_des" type="text" placeholder="CEP" class="form-control" required /><br>
                            </div>
                            <div class="col">
                                <input value="Buscar Cep" id="buscaCepdes" type="button"  class="btn btn-primary" required/><br>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-8">
                                <input id="endereco_des" name="endereco_des" type="text" placeholder="Endereço completo" class="form-control" required/><br>
                            </div>
                            <div class="col-4">
                                <input id="numero_des" name="numero_des" type="text" placeholder="Numero" class="form-control" required/><br>
                            </div>
                        </div>
                        
                        <div class="form-row">
                            <div class="col">
                                <input id="bairro_des" name="bairro_des" type="text" placeholder="Bairro" class="form-control" required /><br>
                            </div>
                            <div class="col">
                                <input id="cidade_des" name="cidade_des" type="text" placeholder="Cidade" class="form-control" required /><br>
                            </div>
                            <div class="col-2">
                                <input id="uf_des" name="uf_des" type="text" placeholder="UF" class="form-control" required /><br>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col">
                                <input name="telefone_des" type="text" placeholder="Telefone" class="form-control"  /><br>
                            </div>
                            <div class="col">
                                <input name="taxa_des" type="number" step="0.01" placeholder="Taxa de Descarga" class="form-control"  /><br>
                            </div>
                        </div>
                              
						
						<button class="btn btn-success" type="submit" style="float: right">Cadastrar</button>
						<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
					</form>
				</div>
				  </div>
			</div>
		  </div>
        
        <script>
        $(document).ready(function () {
            $("#buscaCepdes").click(function(){

            //Nova variável "cep" somente com dígitos.
            var cep = $("#cep_des").val().replace(/\D/g, '');

            //Verifica se campo cep possui valor informado.
            if (cep != "") {

                //Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;

                //Valida o formato do CEP.
                if(validacep.test(cep)) {

                //Consulta o webservice viacep.com.br/
                $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                        if (!("erro" in dados)) {
                            //Atualiza os campos com os valores da consulta.
                            $("#endereco_des").val(dados.logradouro);
                            $("#endereco_des").css("background","#eee");
                            $("#bairro_des").val(dados.bairro);
                            $("#bairro_des").css("background","#eee");
                            $("#cidade_des").val(dados.localidade);
                            $("#cidade_des").css("background","#eee");
                        } //end if.
                        else {
                            //CEP pesquisado não foi encontrado.
                            alert("CEP não encontrado.");
                        }
                    });
                } //end if.
                else {
                    alert("formato de CEP inválido.");
                }
            } //end if.
            });
        });
        </script>