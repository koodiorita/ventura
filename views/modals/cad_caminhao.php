	<!-- AddProd -->
    <div class="modal fade" id="AddCaminhao" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Caminhão</h5>
				  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				  </button>
				</div>
				<div class="modal-body">
					<form action="php/cadastra_caminhao.php" method="POST"  >
                        <div class="form-row">
                            <div class="col">
                                <span>Renavam</span>
                                <input type="text" name="renavam" class="form-control" placeholder="Codigo" required/>
                            </div>
                            <div class="col">
                                <span>Vencimento do documento</span>
                                <input type="date" name="venc_doc" id="venc_doc" class="form-control" required>
                            </div>
                        </div></br>
                        <div class="form-row">
                            <div class="col">
                                <input type="text" name="placa" class="form-control" placeholder="Placa do Caminhão" required/>
                            </div>
                            <div class="col">
                                <input type="text" name="chassi" class="form-control" placeholder="Nº Chassi" required/>
                            </div>
                        </div></br>
                        <div class="form-row">
                            <div class="col">
                                <input type="text" name="marca" class="form-control" placeholder="Marca" required/>
                            </div>
                            <div class="col">
                                <input type="text" name="modelo" class="form-control" placeholder="Modelo" required/>
                            </div>
                        </div></br>

                        <div class="form-row">
                            <div class="col">
                                <input type="text" name="ano-fab" class="form-control" placeholder="Ano Fab" required/>
                            </div>
                            <div class="col">
                                <input type="number" name="ano-mod" class="form-control" placeholder="Ano Mod" required/>
                            </div>
                        </div></br>

                        <div class="form-row">
                            <div class="col">
                                <input type="text" name="cor" class="form-control" placeholder="Cor Predominante" required/>
                            </div>
                        </div></br>
                        
                        <div class="form-row">
                            <div class="col">
                                <input type="text" name="km" class="form-control" placeholder="Km Rodados" required/>
                            </div>
                        </div></br>

                        <div class="col">
                          <label style="color:grey;">Carreta: </label>
                          <div class="col">
                              <input name="radio_carreta" value="1" type="radio" data-toggle="collapse" data-target="#collapseCarreta"/> Sim<br>
                              <input name="radio_carreta" value="0" type="radio" data-toggle="collapse" data-target="#collapseCarreta" checked/> Não <br>
                          </div>
                          <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                              <div id="collapseCarreta" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="form-row">
                                        <div class="col">
                                            <input type="text" name="placa_carreta" id="placa_carreta" class="form-control" placeholder="Placa">
                                        </div>
                                        <div class="col">
                                            <input type="text" name="chassi_carreta" id="chassi_carreta" class="form-control" placeholder="Nº Chassi">
                                        </div>
                                        <div class="col">
                                            <input type="text" name="modelo_carreta" id="modelo_carreta" class="form-control" placeholder="Modelo">
                                        </div>
                                    </div>
                                    <br>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        
						<button class="btn btn-success" type="submit" style="float: right">Cadastrar</button>
						<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
					</form>
				</div>
				  </div>
			</div>
		  </div>
        
      