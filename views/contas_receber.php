<?php 
session_start();

require_once("../conn/conexao.php");

if(!empty($_SESSION['43f6892b19a725cbb5c4048f88337c79'])){
	$usuario_id = $_SESSION['43f6892b19a725cbb5c4048f88337c79'];
}else{
	header('Location: login.php');
}

$sql = "SELECT 
			id,
			vencimento,
			valor_parcela,
			tipo,
			responsavel,
			status
		FROM 
		contas_receber
			where 
			month(vencimento) = month(now()) and year(vencimento) = year(now())
 		";

$res = mysqli_query($conn,$sql);

$sqlBanco = "SELECT * FROM banco";
$resBanco = mysqli_query($conn,$sqlBanco);

?>   
<style>
.onoff input.toggle {
				display: none;
			}

			.onoff input.toggle + label {
				display: inline-block;
				position: relative;
				box-shadow: inset 0 0 0px 1px #d5d5d5;
				height: 20px;
				width: 40px;
				border-radius: 30px;
			}

			.onoff input.toggle + label:before {
				content: "";
				display: block;
				height: 20px;
				width: 40px;
				border-radius: 30px;
				background: rgba(19, 191, 17, 0);
				transition: 0.1s ease-in-out;
			}

			.onoff input.toggle + label:after {
				content: "";
				position: absolute;
				height: 20px;
				width: 20px;
				top: 0;
				left: 0px;
				border-radius: 30px;
				background: #fff;
				box-shadow: inset 0 0 0 1px rgba(0, 0, 0, 0.2), 0 2px 4px rgba(0, 0, 0, 0.2);
				transition: 0.1s ease-in-out;
			}

			.onoff input.toggle:checked + label:before {
				width: 40px;
				background: #13bf11;
			}

			.onoff input.toggle:checked + label:after {
				left: 20px;
				box-shadow: inset 0 0 0 1px #13bf11, 0 2px 4px rgba(0, 0, 0, 0.2);
			}
			.xx{
				float: right;
				background: #ccc;
				border-radius: 200px;
				width:14px;
				height: 13px;
				color: white;
				text-align: center;
				font-size: 10px;
			}
			.xx:hover{
				background: #777;
				cursor: pointer
			}
			.dataTables_wrapper .dataTables_filter input{
				border-radius: 10px;
				border: 1px solid #ccc;
				outline-style: none;
			}
</style>
   <div class="container-fluid">



          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
			<div class="form-row">
              		<div class="col"><h4 class="m-0 font-weight-bold text-primary">Contas à Receber</h4></div>
			  		  
					  <div class="col-2"><input type="date" id="filtro-data1" class="form-control" /></div>
					  <span style="align-self: center;">até</span>
					  <div class="col-2"><input type="date" id="filtro-data2" class="form-control" /></div>
					  <div class="col-2"><button  style="float: right;margin-left: 10px" class=" btn btn-success" onclick="buscar()" >Buscar</button></div>
				  </div>
			  	
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTableReceber" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Vencimento</th>
                      <th>Valor</th>
                      <th>Pagamento / Tipo</th>
                      <th>Responsavel</th>
                      <th width="10%">Receber</th>
                      <th width="10%">Anular</th>
                    </tr>
                  </thead>
                  
                  <tbody>
						<?php
							$valor_total = 0;
						while($row = mysqli_fetch_array($res)) { 
								$valor_total += $row['valor_parcela'];
							?>
							<tr>
								<td><?php echo date('d/m/Y',strtotime($row['vencimento']));?></td>	
                                <td><?php echo "R$ ".number_format($row['valor_parcela'], 2, '.', '');?></td>
                                <td><?php echo $row['tipo'];?></td>
                                <td><?php echo $row['responsavel'];?></td>
								<?php if($row['status'] == 0) { ?>
								<td><center><button class="btn btn-primary btn-circle" onclick="receber(<?php echo $row['id'];?>)" ><i class="fas fa-download" ></i></button></center></td>
								<td><center><button class="btn btn-danger btn-circle" onclick="cancel(<?php echo $row['id'];?>)" ><i class="fas fa-window-close" ></i></button></center></td>
								<?php }else if($row['status']==1){?>
								<td>Recebido</td>
								<td>Não é possível</td>
								<?php }else if($row['status']==2){?>
								<td>Anulado</td>
								<td>Já anulado</td>
								<?php } ?>
							</tr>
						<?php }?>	
                  </tbody>
				  <tfoot>
                    <tr>
                      <th>Vencimento</th>
                      <th><?php echo "R$ ".number_format($valor_total, 2, '.', '');?></th>
                      <th>Pagamento / Tipo</th>
                      <th>Responsavel</th>
                      <th width="10%">Receber</th>
					  <th width="10%">Anular</th>
                    </tr>
                  </tfoot>
                </table>
				
              </div>
            </div>
          </div>
        </div>
	
		<div class="modal fade" id="receberConta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Receber Conta</h5>
				  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				  </button>
				</div>
				<div class="modal-body">
					<form action="php/receber_conta.php" method="POST" enctype="multipart/form-data"  >
						<input id="id_conta_receber" name="id_conta_receber" type="hidden" value = "" />
						<div class="form-row">
							<span>Insira o comprovante</span><br>
							<input type="file" name="arquivo" class="form-control"><br>
						</div><br>
                        <div class="form-row">
							<select name="banco" id="banco" class="form-control">
								<option value="">Selecione o banco</option>
								<?php while ($row = mysqli_fetch_array($resBanco)) { ?>

								<option value="<?=$row['id']?>"><?=$row['nome']?></option>
									
								<?php } ?>
							</select>
						</div><br>
						<button class="btn btn-success" type="submit" style="float: right">Pagar</button>
						<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
					</form>
				</div>
				  </div>
			</div>
		  </div>
		
		<script>
			$(document).ready(function() {
                $('#dataTableReceber').DataTable( {
                });
            });
                    
			function receber(id){

				$('#id_conta_receber').val(id);
				$('#receberConta').modal('show');
				
			}

			function cancel(id){
				var resp = confirm("Deseja anular essa conta ?");
  
				if(resp == true){
					$.get( "php/anular_conta.php?id="+id, function( data ) {
		                location.reload();
					});
				}
			}
			function buscar(){
				var data1 = $("#filtro-data1").val();
				var data2 = $("#filtro-data2").val();

				$.get( "php/filtro_data.php?ini="+data1+"&fim="+data2, function( data ) {
				     $("#dataTableReceber").html(data);
				});
			}
		</script>