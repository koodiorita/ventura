<?php 
session_start();

require_once("../conn/conexao.php");


if(!empty($_SESSION['43f6892b19a725cbb5c4048f88337c79'])){
	$usuario_id = $_SESSION['43f6892b19a725cbb5c4048f88337c79'];
}else{
	header('Location: login.php');
}


$sql = "select * from destino";
$res = mysqli_query($conn,$sql);



	
?>   
<style>
.onoff input.toggle {
				display: none;
			}

			.onoff input.toggle + label {
				display: inline-block;
				position: relative;
				box-shadow: inset 0 0 0px 1px #d5d5d5;
				height: 20px;
				width: 40px;
				border-radius: 30px;
			}

			.onoff input.toggle + label:before {
				content: "";
				display: block;
				height: 20px;
				width: 40px;
				border-radius: 30px;
				background: rgba(19, 191, 17, 0);
				transition: 0.1s ease-in-out;
			}

			.onoff input.toggle + label:after {
				content: "";
				position: absolute;
				height: 20px;
				width: 20px;
				top: 0;
				left: 0px;
				border-radius: 30px;
				background: #fff;
				box-shadow: inset 0 0 0 1px rgba(0, 0, 0, 0.2), 0 2px 4px rgba(0, 0, 0, 0.2);
				transition: 0.1s ease-in-out;
			}

			.onoff input.toggle:checked + label:before {
				width: 40px;
				background: #13bf11;
			}

			.onoff input.toggle:checked + label:after {
				left: 20px;
				box-shadow: inset 0 0 0 1px #13bf11, 0 2px 4px rgba(0, 0, 0, 0.2);
			}
			.xx{
				float: right;
				background: #ccc;
				border-radius: 200px;
				width:14px;
				height: 13px;
				color: white;
				text-align: center;
				font-size: 10px;
			}
			.xx:hover{
				background: #777;
				cursor: pointer
			}
			.dataTables_wrapper .dataTables_filter input{
				border-radius: 10px;
				border: 1px solid #ccc;
				outline-style: none;
			}
</style>
   <div class="container-fluid">



          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h4 class="m-0 font-weight-bold text-primary">Destinos
				<button  style="float: right;margin-left: 10px" class=" btn btn-success" data-toggle="modal" data-target="#AddDestino" >Adicionar</button>
				
			  </h4>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Nome</th>
                      <th>Endereço</th>
                      <th>Telefone</th>
                      <th>Taxa</th>
                      <th width="10%">Editar</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Nome</th>
                      <th>Endereço</th>
                      <th>Telefone</th>
                      <th>Taxa</th>
                      <th width="10%">Editar</th>
                    </tr>
                  </tfoot>
                  <tbody>
						<?php
							
						while($row = mysqli_fetch_array($res)) {
							?>
							<tr>
								<td><?php echo $row['razao_social'];?></td>
								<td><?php echo $row['endereco'].", ".$row['numero']." - ".$row['bairro']." ". $row['cidade']. " ".$row['uf'];?></td>
								<td><?php echo $row['telefone'];?></td>
								<td><?php echo "R$ ".number_format($row['taxa_descarga'],2,".","") ;?></td>
								<td><center><button class="btn btn-warning btn-circle" onclick="edit_destino(<?php echo $row['id'];?>)" ><i class="fas fa-edit" ></i></button></center></td>
							</tr>
						<?php }?>	
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
	
			<!-- AddProd -->
			<div class="modal fade" id="EditDestino" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Editar de Fornecedor</h5>
				  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				  </button>
				</div>
				<div class="modal-body">
					<form action="php/edita_destino.php" method="POST"  >
							<input type="hidden" id="id_des_edit" name="id_des_edit" >
                        <div class="form-row">
                            <div class="col">
                                <input name="razao_des_edit" id="razao_des_edit" type="text" placeholder="Nome da Empresa" class="form-control" required /><br>
                            </div>
							<div class="col">
                                <input name="cnpj_des_edit" id="cnpj_des_edit" type="text" placeholder="CPF / CNPJ" class="form-control"  /><br>
                            </div>
                        </div>
                            
                        <div class="form-row">
                            <div class="col-4">
                                <input id="cep_des_edit" name="cep_des_edit" type="text" placeholder="CEP" class="form-control" required /><br>
                            </div>
                            <div class="col">
                                <input value="Buscar Cep" id="buscaCepDes_edit" type="button"  class="btn btn-primary" /><br>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-8">
                                <input id="endereco_des_edit" name="endereco_des_edit" type="text" placeholder="Endereço completo" class="form-control" required/><br>
                            </div>
                            <div class="col-4">
                                <input id="numero_des_edit" name="numero_des_edit" type="text" placeholder="Numero" class="form-control" required/><br>
                            </div>
                        </div>
                        
                        <div class="form-row">
                            <div class="col">
                                <input id="bairro_des_edit" name="bairro_des_edit" type="text" placeholder="Bairro" class="form-control" required /><br>
                            </div>
                            <div class="col">
                                <input id="cidade_des_edit" name="cidade_des_edit" type="text" placeholder="Cidade" class="form-control" required /><br>
                            </div>
							<div class="col-2">
                                <input id="uf_des_edit" name="uf_des_edit" type="text" placeholder="UF" class="form-control" required /><br>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col">
                                <input name="telefone_des_edit" id="telefone_des_edit" type="text" placeholder="Telefone" class="form-control" required />
                            </div>
                            <div class="col">
                                <input name="taxa_des_edit" id="taxa_des_edit" type="text" placeholder="Taxa de Descarga" class="form-control" /></br>
                            </div>
                            
                        </div>
                                
						
						<button class="btn btn-success" type="submit" style="float: right">Alterar</button>
						<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
					</form>
				</div>
				  </div>
			</div>
		  </div>
		
		<script>
			$(document).ready(function() {
                $('#dataTable').DataTable( {
                });
            });
                    
		
		function edit_destino(id){
			$.get( "php/get_destino.php?id_destino="+id, function( data ) {
					 var json = JSON.parse(data);
					$( "#id_des_edit" ).val( id );
					$( "#cep_des_edit" ).val( json[0].cep );
					$( "#razao_des_edit" ).val( json[1].razao_social );
					$( "#cnpj_des_edit" ).val( json[2].cnpj );
					$( "#taxa_des_edit" ).val( json[3].taxa );
					$( "#cidade_des_edit" ).val( json[4].cidade );
					$( "#endereco_des_edit" ).val( json[5].endereco );
					$( "#bairro_des_edit" ).val( json[6].bairro );
					$( "#numero_des_edit" ).val( json[7].numero );
					$( "#uf_des_edit" ).val( json[8].uf );
					$( "#telefone_des_edit" ).val( json[9].telefone );
					
					$('#EditDestino').modal('show');

				});
				
		}
		</script>

<script>
        $(document).ready(function () {
            $("#buscaCepDes_edit").click(function(){

            //Nova variável "cep" somente com dígitos.
            var cep = $("#cep_des_edit").val().replace(/\D/g, '');

            //Verifica se campo cep possui valor informado.
            if (cep != "") {

                //Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;

                //Valida o formato do CEP.
                if(validacep.test(cep)) {

                //Consulta o webservice viacep.com.br/
                $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                        if (!("erro" in dados)) {
                            //Atualiza os campos com os valores da consulta.
                            $("#endereco_des_edit").val(dados.logradouro);
                            $("#endereco_des_edit").css("background","#eee");
                            $("#bairro_des_edit").val(dados.bairro);
                            $("#bairro_des_edit").css("background","#eee");
                            $("#cidade_des_edit").val(dados.localidade);
                            $("#cidade_des_edit").css("background","#eee");
                        } //end if.
                        else {
                            //CEP pesquisado não foi encontrado.
                            alert("CEP não encontrado.");
                        }
                    });
                } //end if.
                else {
                    alert("Formato de CEP inválido.");
                }
            } //end if.
            });
        });
        </script>