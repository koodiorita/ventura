<?php 
session_start();
include_once('../conn/conexao.php');

$sql = "select 
          v.id,
          c.modelo,
          c.marca,
          c.placa,
          d.cidade,
          f.nome as motorista,
          cli.razao_social
        from 
          viagem as v
          inner join caminhao as c on
          v.id_caminhao = c.id
          inner join cliente as cli on
          v.id_cliente = cli.id
          inner join funcionario as f on
          v.id_motorista = f.id
          inner join destino as d on
          v.id_destino = d.id
        where 
          v.status = 0 and f.funcao = 1";
$res = mysqli_query($conn,$sql);

?>
<div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
          <h1 class="h3 mb-0 text-gray-800">Viagens</h1> 
          <button class="btn btn-success" data-toggle="modal" data-target="#AddViagem">Nova Viagem</button>
        </div>

        <!-- Content Row -->
        <div class="row">

          <!-- 
           - - - - -  
           VALOR CAIXA  
           - - - - -
         -->
          <?php 
            $n = 0;
            while($row = mysqli_fetch_array($res)){
              $n++;
          ?>
          <div class="col-xl-4 col-md-6 mb-4 zoom">
            <div class="card border-left-primary shadow h-100 py-2">
              <div class="card-body">
                <div class="row no-gutters align-items-center">
                  <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-2">Viagem <?php echo $n;?> 
                        
                        <i class="fas fa-truck fa-1x text-gray-300" ></i>
                       
                          <a href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="float: right">
                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                          </a>
                          <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
              
                            
                             <a class="dropdown-item" style="cursor: pointer" onclick="editar_viagem(<?php echo $row['id'];?>)">Editar</a>
                           
                            </div>
                          </div>
                    <div class="h6 mb-0 font-weight-bold text-gray-800" id="caixa-show">
                      <div style="font-size: 13px">
                        <label style="display: block">Caminhão:  <?php echo $row['modelo']." / ".$row['marca'];?></label>
                        <label style="display: block">Placa:     <?php echo $row['placa'];?></label>
                        <label style="display: block">Destino:   <?php echo $row['cidade'];?></label>
                        <label style="display: block">Motorista: <?php echo $row['motorista'];?></label>
                        <label style="display: block">Cliente:   <?php echo $row['razao_social'];?></label>
                      </div>
                    </div>
                  </div>
                </div>
                <button class="btn btn-primary" onclick="fechar_viagem(<?php echo $row['id']; ?>)" >Fechar Viagem</button>
              </div>
            </div>
          </div>
            <?php }?>

        </div>             

        </div>
		   </div>
           

           	<!-- AddProd -->
    <div class="modal fade" id="EditAnotacao" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Nova Anotação</h5>
				  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				  </button>
				</div>
				<div class="modal-body">
					<form action="php/edita_anotacao.php" method="POST"  >
            <input type="hidden" class="form-control" name="id_anotacao_edit" id="id_anotacao_edit" placeholder="Assunto" ><br>
            <input type="text" class="form-control" name="assunto_edit" id="assunto_edit" placeholder="Assunto" ><br>
            <textarea name="mensagem_edit" id="mensagem_edit" class="form-control" height="200px" placeholder="Mensagem ......"></textarea><br>
						<button class="btn btn-success" type="submit" style="float: right">Alterar</button>
						<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
					</form>
				</div>
				  </div>
			</div>
		</div>
        
            	<!-- AddProd -->
    <div class="modal fade" id="AddViagem" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Nova Viagem</h5>
				  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">×</span>
				  </button>
				</div>
				<div class="modal-body">
					<form action="php/cadastra_viagem.php" method="POST"  >

            <div class="form-row">
              <div class="col">Caminhão</div>
              <div class="col">Motorista</div>
            </div>

            <div class="form-row">
              <div class="col">
                <select name="caminhao" id="caminhao" class="form-control" required>
                    <option value=""></option>
                    <?php
                      $sql = "select * from caminhao";
                      $res = mysqli_query($conn,$sql);

                      while($row = mysqli_fetch_array($res)){ ?>
                      <option value="<?php echo $row['id'];?>"><?php echo $row['placa']."/".$row['marca'];?></option>
                      <?php }?>
                </select>
              </div>
              <div class="col">
                <select name="motorista" id="motorista" class="form-control" required>
                    <option value=""></option>
                    <?php
                      $sql = "select * from funcionario where funcao = 1";
                      $res = mysqli_query($conn,$sql);

                      while($row = mysqli_fetch_array($res)){ ?>
                      <option value="<?php echo $row['id'];?>"><?php echo $row['nome']; ?></option>
                      <?php }?>
                </select>
              </div>
            </div><br>

            <div class="form-row">
              <div class="col">Cliente</div>
              <div class="col">Destino</div>
            </div>

            <div class="form-row">              
              <div class="col">
                <select name="cliente" id="cliente" class="form-control" required>
                    <option value=""></option>
                    <?php
                      $sql = "select * from cliente";
                      $res = mysqli_query($conn,$sql);

                      while($row = mysqli_fetch_array($res)){ ?>
                      <option value="<?php echo $row['id'];?>"><?php echo $row['razao_social'];?></option>
                      <?php }?>
                </select>
              </div><br>
              <div class="col">
                <select name="destino" id="destino" class="form-control" required>
                    <option value=""></option>
                    <?php
                      $sql = "select * from destino";
                      $res = mysqli_query($conn,$sql);

                      while($row = mysqli_fetch_array($res)){ ?>
                      <option value="<?php echo $row['id'];?>"><?php echo $row['razao_social']." – ". $row['cidade'];?></option>
                      <?php }?>
                </select>
              </div>
            </div><br>
            <div class="form-row">
              <div class="col">OC</div>
              <div class="col">Vale</div>
            </div>
            <div class="form-row">
              <div class="col">
                <input type="number" class="form-control" name="oc" placeholder="Ordem de Carregamento" required/><br>
              </div>
              <div class="col">
                <input type="number" step="0.01" class="form-control" name="vale" placeholder="Vale" required/><br>
              </div>
            </div>
            
            <div class="form-row">
              <div class="col">Tipo de Carga</div>
              <div class="col"></div>
            </div>

            <div class="form-row">
              <div class="col">
                <select class="form-control" name="tipo_carga">
                  <option value="Batida">Batida</option>
                  <option value="Paletizada">Paletizada</option>
                </select>
              </div>
              <div class="col">
                
              </div>
            </div><br>

            <textarea name="obs_viagem" id="obs_viagem" cols="15" rows="5" placeholder="Observação" class="form-control"></textarea><br>
						<button class="btn btn-success" type="submit" style="float: right">Cadastrar</button>
						<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
					</form>
				</div>
				  </div>
			</div>
		</div>




     <div class="modal fade" id="EditViagem" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Editar Viagem</h5>
				  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">×</span>
				  </button>
				</div>
				<div class="modal-body">
					<form action="php/edita_viagem.php" method="POST"  >
          <input type="hidden" id="id_viagem_edit" name="id_viagem_edit">
            <div class="form-row">
              <div class="col">Caminhão</div>
              <div class="col">Motorista</div>
            </div>

            <div class="form-row">
              <div class="col">
                <select name="caminhao_edit" id="caminhao_edit" class="form-control" required>
                    <option id="caminhao_name" >Selecione um Caminhão</option>
                    <?php
                      $sql = "select * from caminhao";
                      $res = mysqli_query($conn,$sql);

                      while($row = mysqli_fetch_array($res)){ ?>
                      <option value="<?php echo $row['id'];?>"><?php echo $row['modelo']."/".$row['marca'];?></option>
                      <?php }?>
                </select>
              </div>
              <div class="col">
                <select name="motorista_edit" id="motorista_edit" class="form-control" required>
                    <option id="motorista_name">Selecione um Motorista</option>
                    <?php
                      $sql = "select * from funcionario where funcao = 1";
                      $res = mysqli_query($conn,$sql);

                      while($row = mysqli_fetch_array($res)){ ?>
                      <option value="<?php echo $row['id'];?>"><?php echo $row['nome']; ?></option>
                      <?php }?>
                </select>
              </div>
            </div><br>

            <div class="form-row">
              <div class="col">Cliente</div>
              <div class="col">Destino</div>
            </div>

            <div class="form-row">              
              <div class="col">
                <select name="cliente_edit" id="cliente_edit" class="form-control" required>
                    <option id="cliente_name">Selecione um Cliente</option>
                    <?php
                      $sql = "select * from cliente";
                      $res = mysqli_query($conn,$sql);

                      while($row = mysqli_fetch_array($res)){ ?>
                      <option value="<?php echo $row['id'];?>"><?php echo $row['razao_social']." – ". $row['cidade'];?></option>
                      <?php }?>
                </select>
              </div><br>
              <div class="col">
                <select name="destino_edit" id="destino_edit" class="form-control" required>
                    <option id="destino_name">Selecione um Destino</option>
                    <?php
                      $sql = "select * from destino";
                      $res = mysqli_query($conn,$sql);

                      while($row = mysqli_fetch_array($res)){ ?>
                      <option value="<?php echo $row['id'];?>"><?php echo $row['razao_social'];?></option>
                      <?php }?>
                </select>
              </div><br>
            </div>
            <div class="form-row">
              <div class="col">OC</div>
              <div class="col">Vale</div>
            </div>
            <div class="form-row">
              <div class="col">
                <input type="number" class="form-control" id="oc_edit" name="oc_edit" placeholder="Ordem de Carregamento" required/><br>
              </div>
              <div class="col">
                <input type="number" step="0.01" class="form-control" id="vale_edit" name="vale_edit" placeholder="Vale" required/><br>
              </div>
            </div>
            
            <div class="form-row">
              <div class="col">Tipo de Carga</div>
              <div class="col"></div>
            </div>

            <div class="form-row">
              <div class="col">
                <select class="form-control" id="tipo_carga_edit" name="tipo_carga_edit" required>
                  <option id="tipo_carga_name"></option>
                  <option value="Batida">Batida</option>
                  <option value="Paletizada">Paletizada</option>
                </select>
              </div>
              <div class="col">
                
              </div>
            </div><br>

            <textarea name="obs_viagem_edit" id="obs_viagem_edit" cols="15" rows="5" placeholder="Observação" class="form-control"></textarea><br>
						<button class="btn btn-success" type="submit" style="float: right">Editar</button>
						<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
					</form>
				</div>
				  </div>
			</div>
		</div>







    <div class="modal fade" id="FecharViagem" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Fechar Viagem</h5>
				  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">×</span>
				  </button>
				</div>
				<div class="modal-body">
					<form action="php/fechar_viagem.php" method="POST" enctype="multipart/form-data" >
            <input type="hidden" id="id_viagem_enc" name="id_viagem_enc">
            <div class="form-row">
              <div class="col">Caminhão</div>
              <div class="col">Motorista</div>
            </div>

            <div class="form-row">
                <div class="col">
                  <input class="form-control" id="caminhao_enc" readonly />
                </div>
                <div class="col">
                  
                  <input class="form-control" id="motorista_enc" readonly />
                </div>
            </div><br>

            <div class="form-row">
              <div class="col">Cliente</div>
              <div class="col">Destino</div>
            </div>

            <div class="form-row">
              <div class="col">
                <input class="form-control" id="cliente_enc" readonly />
              </div>
              <div class="col">
                 <input class="form-control" id="destino_enc" readonly />
              </div>
            </div><br>

            <div class="form-row">
              <div class="col">OC</div>
              <div class="col">Vale</div>
            </div>
            <div class="form-row">
              <div class="col">
                <input type="number" class="form-control" id="oc_enc" name="oc_enc" placeholder="Ordem de Carregamento" readonly /><br>
              </div>
              <div class="col">
                <input type="number" step="0.01" class="form-control" id="vale_enc" name="vale_enc" placeholder="Vale" readonly /><br>
              </div>
            </div></br>
            
            <div class="form-row">
              <div class="col">Tipo de Carga</div>
              <div class="col"></div>
            </div>

            <div class="form-row">
              <div class="col">
                <input id="tipo_carga_enc" class="form-control"  readonly/>
              </div>
              <div class="col">
                
              </div>
            </div></br>
            
            <div class="form-row">
              <div class="col">Valor da Descarga</div>
              <div class="col">Valor do Pedagio</div>
              <div class="col">Valor do Combustivel</div>
              <div class="col">Litros de Combustível</div>
              <div class="col">Valor do Frete</div>
            </div>

            <div class="form-row">
              <div class="col">
                <input id="valor_descarga_enc" name="valor_descarga_enc" class="form-control" placeholder="99.99" required/>
              </div>
              <div class="col">
                <input id="valor_pedagio_enc" name="valor_pedagio_enc" class="form-control" placeholder="99.99" required/>
              </div>
              <div class="col">
                <input id="valor_combustivel_enc" name="valor_combustivel_enc" class="form-control" placeholder="99.99" required/>
              </div>
              <div class="col">
                <input id="qtd_combustivel_enc" name="qtd_combustivel_enc" class="form-control" placeholder="99.99" required/>
              </div>
              <div class="col">
                <input id="valor_frete_enc" name="valor_frete_enc" class="form-control" placeholder="99.99" required/>
              </div>
            </div></br>
            
            <div class="form-row">
              <div class="col">Nº CTE</div>
              <div class="col">Anexo CTE</div>
            </div>

            <div class="form-row">
              <div class="col">
                <input type="number" id="cte_enc" name="cte_enc" class="form-control"  required/>
              </div>
              <div class="col">
                <input type="file" id="arquivo_cte_enc" name="arquivo_cte_enc" class="form-control"  required/>
              </div>
            </div></br>

            <div class="form-row">
              <div class="col">Km Veiculo</div>
              <div class="col">Data Chegada</div>
            </div>

            <div class="form-row">
              <div class="col">
                <input type="number" step="0.01" id="km_enc" name="km_enc" class="form-control"  required/>
              </div>
              <div class="col">
                <input type="date" id="data_chegada_enc" name="data_chegada_enc" class="form-control"  required/>
              </div>
            </div></br>

            <div class="form-row">
              <div class="col">Nº NF</div>
              <div class="col">Anexo Canhoto</div>
            </div>

            <div class="form-row">
              <div class="col">
                <input type="number" id="num_nf_enc" name="num_nf_enc" class="form-control"  required/>
              </div>
              <div class="col">
                <input type="file" id="arquivo_canhoto_enc" name="arquivo_canhoto_enc" class="form-control"  required/>
              </div>
            </div></br>

            <textarea name="obs_viagem_enc" id="obs_viagem_enc" cols="15" rows="5" placeholder="Observação" class="form-control" ></textarea><br>
            
						<button class="btn btn-success" type="submit" style="float: right">Encerrar Viagem</button>
						<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
					</form>
				</div>
				  </div>
			</div>
		</div>
<script>


function ver(){
    if ($('#ver-radio').is(':checked')) {
      $('#ver').removeClass('fas fa-eye-slash text-white');
      $('#ver').addClass('fas fa-eye text-white');
      $('#ver-radio').attr( 'checked', false );

      $('#caixa-show').css('display','none');
      $('#receber-show').css('display','none');
      $('#pagar-show').css('display','none');
      $('#orcamento-show').css('display','none');

      $('#caixa-show2').css('display','block');
      $('#receber-show2').css('display','block');
      $('#pagar-show2').css('display','block');
      $('#orcamento-show2').css('display','block');

    }else{
      $('#ver').removeClass('fas fa-eye text-white');
      $('#ver').addClass('fas fa-eye-slash text-white');
      $('#ver-radio').attr( 'checked', true );

      $('#caixa-show').css('display','block');
      $('#receber-show').css('display','block');
      $('#pagar-show').css('display','block');
      $('#orcamento-show').css('display','block');

      $('#caixa-show2').css('display','none');
      $('#receber-show2').css('display','none');
      $('#pagar-show2').css('display','none');
      $('#orcamento-show2').css('display','none');


    }
    
    
}

function editar_anotacao(id){
    $.get("php/get_anotacao.php?id="+id,function(data){
        var json = JSON.parse(data);
        $( "#id_anotacao_edit" ).val( id );
        $( "#assunto_edit" ).val( json[0].assunto );
        $( "#mensagem_edit" ).val( json[1].mensagem );
        $("#EditAnotacao").modal('show');
    });
}

function editar_viagem(id){
    $.get("php/get_viagem.php?id_viagem="+id,function(data){
        var json = JSON.parse(data);
        $("#id_viagem_edit" ).val( id );
        $("#caminhao_name" ).html( json[0].id_caminhao );
        $("#cliente_name" ).html( json[1].id_cliente );
        $("#motorista_name" ).html( json[2].id_motorista );
        $("#destino_name" ).html( json[3].id_destino );
        $("#oc_edit" ).val( json[4].oc );
        $("#vale_edit" ).val( json[5].vale );
        $("#tipo_carga_name" ).html( json[6].tipo_carga );
        $("#tipo_carga_name" ).val( json[6].tipo_carga );
        $("#obs_viagem_edit" ).val( json[7].obs );

        $("#caminhao_name" ).val( json[8].id_caminhao_c );
        $("#cliente_name" ).val( json[9].id_cliente_c );
        $("#motorista_name" ).val( json[10].id_motorista_c );
        $("#destino_name" ).val( json[11].id_destino_c );


        $("#EditViagem").modal('show');
    });
}

function fechar_viagem(id){
    $.get("php/get_viagem.php?id_viagem="+id,function(data){
        var json = JSON.parse(data);
        $("#id_viagem_enc" ).val( id );
        $("#caminhao_enc" ).val( json[0].id_caminhao );
        $("#cliente_enc" ).val( json[1].id_cliente );
        $("#motorista_enc" ).val( json[2].id_motorista );
        $("#destino_enc" ).val( json[3].id_destino );
        $("#oc_enc" ).val( json[4].oc );
        $("#vale_enc" ).val( json[5].vale );
        $("#tipo_carga_enc" ).val( json[6].tipo_carga );
        $("#obs_viagem_enc" ).val( json[7].obs );
        $("#FecharViagem").modal('show');
    });
}

$(document).ready(function () {
      $('#caminhao').selectize({
          sortField: 'text',
          placeholder: 'Selecione um caminhão'
      });
      $('#motorista').selectize({
          sortField: 'text',
          placeholder: 'Selecione um motorista'
      });
      $('#destino').selectize({
          sortField: 'text',
          placeholder: 'Selecione um destino'
      });
      $('#cliente').selectize({
          sortField: 'text',
          placeholder: 'Selecione um cliente'
      });
      $('#caminhao_edit').selectize({
          sortField: 'text',
          placeholder: 'Selecione um caminhão'
      });
      $('#motorista_edit').selectize({
          sortField: 'text',
          placeholder: 'Selecione um motorista'
      });
      $('#destino_edit').selectize({
          sortField: 'text',
          placeholder: 'Selecione um destino'
      });
      $('#cliente_edit').selectize({
          sortField: 'text',
          placeholder: 'Selecione um cliente'
      });
  });
</script>