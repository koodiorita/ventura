<?php
session_start();

require_once("../conn/conexao.php");


if (!empty($_SESSION['43f6892b19a725cbb5c4048f88337c79'])) {
  $usuario_id = $_SESSION['43f6892b19a725cbb5c4048f88337c79'];
} else {
  header('Location: login.php');
}


$sql = "select * from caminhao";
$res = mysqli_query($conn, $sql);




?>
<style>
  .onoff input.toggle {
    display: none;
  }

  .onoff input.toggle+label {
    display: inline-block;
    position: relative;
    box-shadow: inset 0 0 0px 1px #d5d5d5;
    height: 20px;
    width: 40px;
    border-radius: 30px;
  }

  .onoff input.toggle+label:before {
    content: "";
    display: block;
    height: 20px;
    width: 40px;
    border-radius: 30px;
    background: rgba(19, 191, 17, 0);
    transition: 0.1s ease-in-out;
  }

  .onoff input.toggle+label:after {
    content: "";
    position: absolute;
    height: 20px;
    width: 20px;
    top: 0;
    left: 0px;
    border-radius: 30px;
    background: #fff;
    box-shadow: inset 0 0 0 1px rgba(0, 0, 0, 0.2), 0 2px 4px rgba(0, 0, 0, 0.2);
    transition: 0.1s ease-in-out;
  }

  .onoff input.toggle:checked+label:before {
    width: 40px;
    background: #13bf11;
  }

  .onoff input.toggle:checked+label:after {
    left: 20px;
    box-shadow: inset 0 0 0 1px #13bf11, 0 2px 4px rgba(0, 0, 0, 0.2);
  }

  .xx {
    float: right;
    background: #ccc;
    border-radius: 200px;
    width: 14px;
    height: 13px;
    color: white;
    text-align: center;
    font-size: 10px;
  }

  .xx:hover {
    background: #777;
    cursor: pointer
  }

  .dataTables_wrapper .dataTables_filter input {
    border-radius: 10px;
    border: 1px solid #ccc;
    outline-style: none;
  }

  .show {
    display: block;
  }

  .hide {
    display: none;
  }
</style>
<div class="container-fluid">



  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h4 class="m-0 font-weight-bold text-primary">Caminhão
        <button style="float: right;margin-left: 10px" class=" btn btn-success" data-toggle="modal" data-target="#AddCaminhao">Adicionar</button>

      </h4>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTableCaminhao" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Marca / Modelo</th>
              <th>Ano Fab / Ano Mod</th>
              <th>Placa</th>
              <th>Carreta</th>
              <th width="23%">Documentação da carreta (Placa/Modelo/Chassi)</th>
              <th width="10%">Editar</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>Marca / Modelo</th>
              <th>Ano Fab / Ano Mod</th>
              <th>Placa</th>
              <th>Carreta</th>
              <th width="23%">Documentação da carreta (Placa/Modelo/Chassi)</th>
              <th width="10%">Editar</th>
            </tr>
          </tfoot>
          <tbody>
            <?php

            while ($row = mysqli_fetch_array($res)) {
              if ($row['carreta'] == 1) {
                $carreta = "Sim";
                $doc_carreta = $row['placa_carreta'] . " / " . $row['modelo_carreta'] . " / " . $row['chassi_carreta'];
              } else {
                $carreta = "Não";
                $doc_carreta = "Não possui carreta";
              }
            ?>
              <tr>
                <td><?php echo $row['marca'] . "/" . $row['modelo']; ?></td>
                <td><?php echo $row['ano_fab'] . "/" . $row['ano_mod']; ?></td>
                <td><?php echo $row['placa']; ?></td>
                <td><?= $carreta; ?></td>
                <td><?= $doc_carreta; ?></td>
                <td>
                  <center><button class="btn btn-warning btn-circle" onclick="edit_caminhao(<?php echo $row['id']; ?>)"><i class="fas fa-edit"></i></button></center>
                </td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>

<!-- AddProd -->
<div class="modal fade" id="EditCaminhao" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Editar Caminhão</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="php/edita_caminhao.php" method="POST" id="form-editar-caminhao">
          <input type="hidden" id="id_caminhao_edit" name="id_caminhao_edit">
          <div class="form-row">
            <div class="col">
              <span>Renavam</span>
              <input type="text" id="renavam_edit" name="renavam_edit" class="form-control" placeholder="Cod" required />
            </div>
            <div class="col">
              <span>Vencimento do documento</span>
              <input type="date" id="venc_doc_edit" name="venc_doc_edit" class="form-control" required />
            </div>
          </div></br>
          <div class="form-row">
            <div class="col">
              <input type="text" id="placa_edit" name="placa_edit" class="form-control" placeholder="Placa do Caminhão" required />
            </div>
            <div class="col">
              <input type="text" id="chassi_edit" name="chassi_edit" class="form-control" placeholder="Nº Chassi" required />
            </div>
          </div></br>
          <div class="form-row">
            <div class="col">
              <input type="text" id="marca_edit" name="marca_edit" class="form-control" placeholder="Marca" required />
            </div>
            <div class="col">
              <input type="text" id="modelo_edit" name="modelo_edit" class="form-control" placeholder="Modelo" required />
            </div>
          </div></br>

          <div class="form-row">
            <div class="col">
              <input type="text" id="ano-fab_edit" name="ano-fab_edit" class="form-control" placeholder="Ano Fab" required />
            </div>
            <div class="col">
              <input type="number" id="ano-mod_edit" name="ano-mod_edit" class="form-control" placeholder="Ano Mod" required />
            </div>
          </div></br>

          <div class="form-row">
            <div class="col">
              <input type="text" id="cor_edit" name="cor_edit" class="form-control" placeholder="Cor Predominante" required />
            </div>
          </div></br>

          <div class="form-row">
            <div class="col">
              <input type="text" id="km_edit" name="km_edit" class="form-control" placeholder="Km Rodados" required />
            </div>
          </div></br>

          <div class="col">
            <label style="color:grey;">Carreta: </label>
            <div class="col">
              <input name="radio_carreta_edit" id="radio_carreta_1" onchange="carreta(this)" value="1" type="radio"> Sim <br>
              <input name="radio_carreta_edit" id="radio_carreta_0" onchange="carreta(this)" value="0" type="radio"> Não <br>
            </div>
            <div id="entrada_radio_nao" class="hide">
              <br>
              <label style="color:grey;">Sem carreta</label>
              <br>
            </div>
            <div id="entrada_radio_sim" class="hide">
              <br>
              <div class="form-row">
                <div class="col">
                  <input type="text" name="placa_carreta_edit" id="placa_carreta_edit" class="form-control" placeholder="Placa">
                </div>
                <div class="col">
                  <input type="text" name="chassi_carreta_edit" id="chassi_carreta_edit" class="form-control" placeholder="Nº Chassi">
                </div>
                <div class="col">
                  <input type="text" name="modelo_carreta_edit" id="modelo_carreta_edit" class="form-control" placeholder="Modelo">
                </div>
              </div><br>
            </div>
          </div>

          <button class="btn btn-success" type="button" onclick="btn_editar()" style="float: right">Editar</button>
          <button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
        </form>
      </div>
    </div>
  </div>
</div>


<script>
  $(document).ready(function() {
    $('#dataTableCaminhao').DataTable({});
  });


  function edit_caminhao(id) {
    $('#id_caminhao_edit').val(id);
    $.get("php/get_caminhao.php?id_caminhao=" + id, function(data) {
      var json = JSON.parse(data);
      $('#renavam_edit').val(json[0].renavam);
      $('#venc_doc_edit').val(json[1].vencimento_doc);
      $('#placa_edit').val(json[2].placa);
      $('#chassi_edit').val(json[3].chassi);
      $('#marca_edit').val(json[4].marca);
      $('#modelo_edit').val(json[5].modelo);
      $('#ano-fab_edit').val(json[6].ano_fab);
      $('#ano-mod_edit').val(json[7].ano_mod);
      $('#cor_edit').val(json[8].cor);
      $('#km_edit').val(json[9].km);
      $('#placa_carreta_edit').val(json[10].placa_carreta);
      $('#chassi_carreta_edit').val(json[11].chassi_carreta);
      $('#modelo_carreta_edit').val(json[12].modelo_carreta);
      $('#EditCaminhao').modal('show');

    });
  }

  function btn_editar() {
    if ($('#radio_carreta_1').is(':checked')) {
      var carreta = 1;
    } else {
      var carreta = 0;
    }

    console.log(carreta);

    if (carreta == 1) {
      var placa = $('#placa_carreta_edit').val();
      var chassi = $('#chassi_carreta_edit').val();
      var modelo = $('#modelo_carreta_edit').val();

      if (placa.length > 0 && chassi.length > 0 && modelo.length > 0) {
        $('#form-editar-caminhao').submit();
      } else {
        alert("Campos incompletos");
      }
    } else {
      document.getElementById('placa_carreta_edit').value = '';
      document.getElementById('chassi_carreta_edit').value = '';
      document.getElementById('modelo_carreta_edit').value = '';
      $('#form-editar-caminhao').submit();
    }
  }

  function carreta(obj) {
    if (obj.value == 0) {
      $('#entrada_radio_nao').addClass("show");
      $('#entrada_radio_nao').removeClass("hide");

      $('#entrada_radio_sim').removeClass("show");
      $('#entrada_radio_sim').addClass("hide");

    } else {
      $('#entrada_radio_nao').removeClass("show");
      $('#entrada_radio_nao').addClass("hide");

      $('#entrada_radio_sim').removeClass("hide");
      $('#entrada_radio_sim').addClass("show");

    }
  }
</script>