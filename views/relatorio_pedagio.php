<?php
session_start();

require_once("../conn/conexao.php");


if (!empty($_SESSION['43f6892b19a725cbb5c4048f88337c79'])) {
	$usuario_id = $_SESSION['43f6892b19a725cbb5c4048f88337c79'];
} else {
	header('Location: login.php');
}


$sql  = "SELECT
            p.id,
            p.valor,
            d.razao_social,
            cli.razao_social as nome_cliente,
            p.status
        FROM pedagio as p
            INNER JOIN viagem as v on
                p.id_viagem = v.id
            INNER JOIN cliente as cli on
                cli.id = v.id_cliente
            INNER JOIN destino as d on
                d.id = v.id_destino
		";
$res = mysqli_query($conn, $sql);

$count = mysqli_num_rows($res);



?>
<style>
	.onoff input.toggle {
		display: none;
	}

	.onoff input.toggle+label {
		display: inline-block;
		position: relative;
		box-shadow: inset 0 0 0px 1px #d5d5d5;
		height: 20px;
		width: 40px;
		border-radius: 30px;
	}

	.onoff input.toggle+label:before {
		content: "";
		display: block;
		height: 20px;
		width: 40px;
		border-radius: 30px;
		background: rgba(19, 191, 17, 0);
		transition: 0.1s ease-in-out;
	}

	.onoff input.toggle+label:after {
		content: "";
		position: absolute;
		height: 20px;
		width: 20px;
		top: 0;
		left: 0px;
		border-radius: 30px;
		background: #fff;
		box-shadow: inset 0 0 0 1px rgba(0, 0, 0, 0.2), 0 2px 4px rgba(0, 0, 0, 0.2);
		transition: 0.1s ease-in-out;
	}

	.onoff input.toggle:checked+label:before {
		width: 40px;
		background: #13bf11;
	}

	.onoff input.toggle:checked+label:after {
		left: 20px;
		box-shadow: inset 0 0 0 1px #13bf11, 0 2px 4px rgba(0, 0, 0, 0.2);
	}

	.xx {
		float: right;
		background: #ccc;
		border-radius: 200px;
		width: 14px;
		height: 13px;
		color: white;
		text-align: center;
		font-size: 10px;
	}

	.xx:hover {
		background: #777;
		cursor: pointer
	}

	.dataTables_wrapper .dataTables_filter input {
		border-radius: 10px;
		border: 1px solid #ccc;
		outline-style: none;
	}
</style>
<div class="container-fluid">



	<!-- DataTales Example -->
	<div class="card shadow mb-4">
		<div class="card-header py-3"><button style="float: right;margin-left: 10px" class=" btn btn-success" data-toggle="modal" data-target="#gerarConta">Gerar Conta</button>
			<div class="form-row">
				<div class="col">
					<h4 class="m-0 font-weight-bold text-primary">Relatório Contas de Pedágios</h4>
				</div>
				<div class="col-3"><input type="date" id="filtro-data-pagar-1" class="form-control" /></div>
				<span style="align-self: center;">até</span>
				<div class="col-3"><input type="date" id="filtro-data-pagar-2" class="form-control" /></div>
				<div class="col-2"><button style="float: right;margin-left: 10px" class=" btn btn-success" onclick="buscarPagar()">Buscar</button></div>
			</div>

		</div>


		</h4>
	</div>
	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTableRelatorio" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th width="5%">Check</th>
						<th>Viagem</th>
						<th>Responsavel</th>
						<th>Valor</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$total = 0;
					while ($row = mysqli_fetch_array($res)) {
						$total += $row['valor'];
						if ($row['status'] == 1) {
							$status = "Conta gerada";
						} else if ($row['status'] == 0) {
							$status = "Em aberto";
						}

					?>
						<tr>
							<?php if ($row['status'] == 0) { ?>
								<td>
									<center>
										<input type="checkbox" class="form-control" style="width:25px;" id="pedagio_<?= $row['id'] ?>" value="<?= $row['id'] ?>">
									</center>
								</td>
							<?php } else if ($row['status'] == 1) { ?>
								<td>
									<center>Gerada</center>
								</td>
							<?php } ?>
							<td><?= $row['razao_social'] ?></td>
							<td><?= $row['nome_cliente'] ?></td>
							<td><?= "R$ " . number_format($row['valor'], 2, '.', '') ?></td>
							<td><?= $status ?></td>
						</tr>
					<?php } ?>
				</tbody>
				<tfoot>
					<tr>
						<th width="5%">Check</th>
						<th>Viagem</th>
						<th>Responsavel</th>
						<th>Valor <?= "R$ " . number_format($total, 2, '.', '') ?></th>
						<th>Status</th>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>

</div>



<!-- AddPagarConta -->
<div class="modal fade" id="gerarConta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title font-weight-bold" id="exampleModalLabel">Gerar Conta</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="php/gerar_conta_pedagio.php" method="POST" enctype="multipart/form-data" id="form-gerar">
					<input type="hidden" value="<?= $count ?>" id="count_pedagio" name="count_pedagio">
					<input type="hidden" value="" id="ids_pedagios" name="ids_pedagios">
					<div class="form-row">
						<span>Insira a data de vencimento da conta:</span>
						<input type="date" name="data_vencimento" id="data_vencimento" class="form-control">
					</div>
					<br>
					<button onclick="gerar()" class="btn btn-success" type="button" style="float: right">Gerar</button>
					<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
				</form>
			</div>
		</div>
	</div>
</div>


<script>
	$(document).ready(function() {
		$('#dataTableRelatorio').DataTable({});
	});

	var string_ids = "";

	function gerar() {
		var contagem = $('#count_pedagio').val();

		for (var i = 1; i <= contagem; i++) {
			if ($('#pedagio_' + i).is(':checked')) {
				// 1;3;
				string_ids += $('#pedagio_' + i).val() + ";";
			}
		}

		$('#ids_pedagios').val(string_ids);

		console.log(string_ids);
		console.log(contagem);
		$("#form-gerar").submit();

	}


	function buscarPagar() {
		var data1 = $("#filtro-data-pagar-1").val();
		var data2 = $("#filtro-data-pagar-2").val();

		$.get("php/filtro_data_relatorios.php?tipo=pedagio" + "&ini=" + data1 + "&fim=" + data2, function(data) {
			$("#dataTableRelatorio").html(data);
		});
	}
</script>