<?php
session_start();

require_once("../conn/conexao.php");


if (!empty($_SESSION['43f6892b19a725cbb5c4048f88337c79'])) {
    $usuario_id = $_SESSION['43f6892b19a725cbb5c4048f88337c79'];
} else {
    header('Location: login.php');
}

$sql = "SELECT 
            v.id,
            c.razao_social,
            ca.marca,
            ca.modelo,
            f.nome,
            d.cidade,
            v.valor_frete,
            v.data_cad,
            v.data_fim,
            ca.placa
        FROM 
            viagem as v 
            inner join cliente as c ON
            v.id_cliente = c.id
            inner join caminhao as ca ON
            v.id_caminhao = ca.id
            inner join funcionario as f ON
            v.id_motorista = f.id
            inner join destino as d ON
            v.id_destino = d.id";


$resGeral = mysqli_query($conn, $sql);




?>
<style>
    .onoff input.toggle {
        display: none;
    }

    .onoff input.toggle+label {
        display: inline-block;
        position: relative;
        box-shadow: inset 0 0 0px 1px #d5d5d5;
        height: 20px;
        width: 40px;
        border-radius: 30px;
    }

    .onoff input.toggle+label:before {
        content: "";
        display: block;
        height: 20px;
        width: 40px;
        border-radius: 30px;
        background: rgba(19, 191, 17, 0);
        transition: 0.1s ease-in-out;
    }

    .onoff input.toggle+label:after {
        content: "";
        position: absolute;
        height: 20px;
        width: 20px;
        top: 0;
        left: 0px;
        border-radius: 30px;
        background: #fff;
        box-shadow: inset 0 0 0 1px rgba(0, 0, 0, 0.2), 0 2px 4px rgba(0, 0, 0, 0.2);
        transition: 0.1s ease-in-out;
    }

    .onoff input.toggle:checked+label:before {
        width: 40px;
        background: #13bf11;
    }

    .onoff input.toggle:checked+label:after {
        left: 20px;
        box-shadow: inset 0 0 0 1px #13bf11, 0 2px 4px rgba(0, 0, 0, 0.2);
    }

    .xx {
        float: right;
        background: #ccc;
        border-radius: 200px;
        width: 14px;
        height: 13px;
        color: white;
        text-align: center;
        font-size: 10px;
    }

    .xx:hover {
        background: #777;
        cursor: pointer
    }

    .dataTables_wrapper .dataTables_filter input {
        border-radius: 10px;
        border: 1px solid #ccc;
        outline-style: none;
    }
</style>
<div class="container-fluid">
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h4 class="m-0 font-weight-bold text-primary">Relatório de Viagens
            </h4><br>
            <h4>Filtros</h4>
            <div class="row" style="display:flex;justify-content:space-between;align-items:center;">

                <select class="form-control col-md-2 m-1" id="filter_caminhao">
                    <option value="">Filtrar por Caminhão</option>
                    <?php
                    $sql = "select * from caminhao";
                    $res = mysqli_query($conn, $sql);
                    while ($row = mysqli_fetch_array($res)) { ?>
                        <option value="<?php echo $row['id']; ?>"><?php echo $row['marca'] . " / " . $row['modelo']; ?></option>
                    <?php }
                    ?>
                </select>

                <select class="form-control col-md-2 m-1" id="filter_cliente">
                    <option value="">Filtrar por Cliente</option>
                    <?php
                    $sql = "select * from cliente";
                    $res = mysqli_query($conn, $sql);
                    while ($row = mysqli_fetch_array($res)) { ?>
                        <option value="<?php echo $row['id']; ?>"><?php echo $row['razao_social']; ?></option>
                    <?php }
                    ?>
                </select>
                <select class="form-control col-md-2 m-1" id="filter_motorista">
                    <option value="">Filtrar por Motorista</option>
                    <?php
                    $sql = "select * from funcionario where funcao = 1";
                    $res = mysqli_query($conn, $sql);
                    while ($row = mysqli_fetch_array($res)) { ?>
                        <option value="<?php echo $row['id']; ?>"><?php echo $row['nome']; ?></option>
                    <?php }
                    ?>
                </select>

                <div class="form-row mr-1">
                    <label style="margin-top:12px;" for="filter_data_ini">De:</label>
                    <div class="col"><input type="date" class="form-control  m-1" id="filter_data_ini" /></div>
                    <label style="margin-top:12px;" for="filter_data_fim">Até:</label>
                    <div class="col"><input type="date" class="form-control  m-1" id="filter_data_fim" /></div>
                </div>

                <button class="btn btn-primary m-1" onclick="filtrar_viagem()">Buscar</button>

            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Data Inicio</th>
                            <th>Data Fim</th>
                            <th>Cliente</th>
                            <th>Destino</th>
                            <th>Marca / Modelo / Placa</th>
                            <th>Motorista</th>
                            <th>View</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Data Inicio</th>
                            <th>Data Fim</th>
                            <th>Cliente</th>
                            <th>Destino</th>
                            <th>Marca / Modelo / Placa</th>
                            <th>Motorista</th>
                            <th>View</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php
                        while ($row = mysqli_fetch_array($resGeral)) {
                        ?>
                            <tr>
                                <td><?php echo date('d/m/Y H:i:s', strtotime($row['data_cad'])); ?></td>
                                <td><?= date('d/m/Y H:i:s', strtotime($row['data_fim'])) ?></td>
                                <td><?php echo $row['razao_social']; ?></td>
                                <td><?php echo $row['cidade']; ?></td>
                                <td><?php echo $row['marca'] . " / " . $row['modelo'] . " / " . $row['placa']; ?></td>
                                <td><?php echo $row['nome']; ?></td>
                                <td>
                                    <center><button class="btn btn-primary btn-circle" onclick="visualizaViagem(<?php echo $row['id']; ?>)"><i class="fas fa-edit"></i></button></center>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="VisualizaViagem" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Visualiza Viagem</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="id_viagem_view" name="id_viagem_view">
                <div class="form-row">
                    <div class="col">Caminhão</div>
                    <div class="col">Cliente</div>
                </div>

                <div class="form-row">
                    <div class="col">
                        <input class="form-control" id="caminhao_view" readonly />
                    </div>
                    <div class="col">
                        <input class="form-control" id="cliente_view" readonly />
                    </div>
                </div><br>

                <div class="form-row">
                    <div class="col">Motorista</div>
                    <div class="col">Destino</div>
                </div>

                <div class="form-row">
                    <div class="col">
                        <input class="form-control" id="motorista_view" readonly />
                    </div>
                    <div class="col">
                        <input class="form-control" id="destino_view" readonly />
                    </div>
                </div><br>

                <div class="form-row">
                    <div class="col">OC</div>
                    <div class="col">Vale</div>
                </div>
                <div class="form-row">
                    <div class="col">
                        <input type="number" class="form-control" id="oc_view" name="oc_view" placeholder="Ordem de Carregamento" readonly /><br>
                    </div>
                    <div class="col">
                        <input type="number" step="0.01" class="form-control" id="vale_view" name="vale_view" placeholder="Vale" readonly /><br>
                    </div>
                </div></br>

                <div class="form-row">
                    <div class="col">Tipo de Carga</div>
                    <div class="col"></div>
                </div>

                <div class="form-row">
                    <div class="col">
                        <input id="tipo_carga_view" class="form-control" readonly />
                    </div>
                    <div class="col">

                    </div>
                </div></br>

                <div class="form-row">
                    <div class="col">Valor da Descarga</div>
                    <div class="col">Valor do Pedagio</div>
                    <div class="col">Valor do Combustivel</div>
                    <div class="col">Valor do Frete</div>
                </div>

                <div class="form-row">
                    <div class="col">
                        <input id="valor_descarga_view" name="valor_descarga_view" class="form-control" readonly />
                    </div>
                    <div class="col">
                        <input id="valor_pedagio_view" name="valor_pedagio_view" class="form-control" readonly />
                    </div>
                    <div class="col">
                        <input id="valor_combustivel_view" name="valor_combustivel_view" class="form-control" readonly />
                    </div>
                    <div class="col">
                        <input id="valor_frete_view" name="valor_frete_view" class="form-control" readonly />
                    </div>
                </div></br>

                <div class="form-row">
                    <div class="col">Nº CTE</div>
                    <div class="col">Anexo CTE</div>
                </div>

                <div class="form-row">
                    <div class="col">
                        <input type="number" id="cte_view" name="cte_view" class="form-control" readonly />
                    </div>
                    <div class="col">
                        <input onclick="viewDocs('arquivo_cte')" type="button" id="arquivo_cte_view" name="arquivo_cte_view" class="btn btn-primary" value="Arquivo CTE" />
                    </div>
                </div></br>

                <div class="form-row">
                    <div class="col">Km Veiculo</div>
                    <div class="col">Data Chegada</div>
                </div>

                <div class="form-row">
                    <div class="col">
                        <input type="number" step="0.01" id="km_view" name="km_view" class="form-control" readonly />
                    </div>
                    <div class="col">
                        <input type="date" id="data_chegada_view" name="data_chegada_view" class="form-control" readonly />
                    </div>
                </div></br>

                <div class="form-row">
                    <div class="col">Nº NF</div>
                    <div class="col">Anexo Canhoto</div>
                </div>

                <div class="form-row">
                    <div class="col">
                        <input type="number" id="num_nf_view" name="num_nf_view" class="form-control" readonly />
                    </div>
                    <div class="col">
                        <input type="button" onclick="viewDocs('arquivo_canhoto')" id="arquivo_canhoto_view" name="arquivo_canhoto_view" class="btn btn-primary" value="Arquivo Canhoto" />
                    </div>
                </div></br>

                <textarea name="obs_viagem_view" id="obs_viagem_view" cols="15" rows="5" placeholder="Observação" class="form-control" readonly></textarea><br>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function() {
        $('#dataTable').DataTable({});
    });

    function visualizaViagem(id) {
        $.get("php/get_viagem.php?id_viagem=" + id, function(data) {
            var json = JSON.parse(data);
            $("#id_viagem_view").val(id);
            $("#caminhao_view").val(json[0].id_caminhao);
            $("#cliente_view").val(json[1].id_cliente);
            $("#motorista_view").val(json[2].id_motorista);
            $("#destino_view").val(json[3].id_destino);
            $("#oc_view").val(json[4].oc);
            $("#vale_view").val("R$ " + json[5].vale);
            $("#tipo_carga_view").val(json[6].tipo_carga);
            $("#obs_viagem_view").val(json[7].obs);

            $("#valor_descarga_view").val("R$ " + json[12].valor_descarga_c);
            $("#valor_pedagio_view").val("R$ " + json[13].valor_pedagio_c);
            $("#valor_combustivel_view").val("R$ " + json[14].valor_combustivel_c);
            $("#valor_frete_view").val("R$ " + json[15].valor_frete_c);
            $("#cte_view").val(json[16].cte_c);
            $("#km_view").val(json[17].km_c);
            $("#data_chegada_view").val(json[18].data_chegada_c);
            $("#num_nf_view").val(json[19].numero_nota_c);

            $("#VisualizaViagem").modal('show');
        });
    }

    function viewDocs(tipo) {
        var id = $("#id_viagem_view").val();
        window.open("views/modals/view_viagem.php?id=" + id + "&tipo=" + tipo, '_BLANK');
    }

    function filtrar_viagem() {
        var filter_caminhao = $("#filter_caminhao").val();
        var filter_cliente = $("#filter_cliente").val();
        var filter_motorista = $("#filter_motorista").val();
        var filter_data_ini = $("#filter_data_ini").val();
        var filter_data_fim = $("#filter_data_fim").val();

        $.get("php/filtro_viagem.php?filter_caminhao=" + filter_caminhao + "&filter_cliente=" + filter_cliente + "&filter_motorista=" + filter_motorista + "&filter_data_ini=" + filter_data_ini + "&filter_data_fim=" + filter_data_fim, function(data) {
            $("#dataTable").empty();

            $("#dataTable").html(data);
        });
    }
</script>