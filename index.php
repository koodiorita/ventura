<?php
require_once('conn/conexao.php');
session_start();

$n = 0;



if (!empty($_SESSION['43f6892b19a725cbb5c4048f88337c79'])) {
  $usuario_id = $_SESSION['43f6892b19a725cbb5c4048f88337c79'];
  $tela_inicial = $_SESSION['tela_inicial'];
} else {
  header('Location: login.php');
}

$sql = "select * from user where id = $usuario_id";
$res = mysqli_query($conn, $sql);

while ($row = mysqli_fetch_array($res)) {
  $nome   = $row['nome'];
  $email  = $row['email'];
  $image  = $row['avatar'];
  if (strlen($image) > 2) {
    $image  = $row['avatar'];
  } else {
    $image = "img/avatar.png";
  }
}

$sql = "SELECT * FROM user_permission WHERE id_user = $usuario_id";
$res = mysqli_query($conn, $sql);
$telas = array();
while ($row = mysqli_fetch_array($res)) {
  array_push($telas, $row['permission']);
}

//COUNT PARA AS NOTIFICAÇÕES
$qtd_notificacoes = 0;
$qtd_cp = 0;
$qtd_cr = 0;
$qtd_cnh = 0;
$qtd_doc = 0;

//CONTAS A PAGAR VENCIDAS
$sql = "SELECT * FROM contas_pagar WHERE vencimento <= now() AND status=0";
$res_cp = mysqli_query($conn, $sql);
$qtd_cp = mysqli_num_rows($res_cp);

//CONTAS A RECEBER VENCIDAS
$sql = "SELECT * FROM contas_receber WHERE vencimento <= now() AND status=0";
$res_cr = mysqli_query($conn, $sql);
$qtd_cr = mysqli_num_rows($res_cr);

//VENCIMENTO DE CNH DE FUNCIONARIO
$sql = "SELECT * FROM funcionario WHERE vencimento_cnh <= now()";
$res_cnh = mysqli_query($conn, $sql);
$qtd_cnh = mysqli_num_rows($res_cnh);

//VENCIMENTO DE DOC DE VEICULO
$sql = "SELECT * FROM caminhao WHERE vencimento_doc <= now()";
$res_doc = mysqli_query($conn, $sql);
$qtd_doc = mysqli_num_rows($res_doc);

$qtd_notificacoes = $qtd_cp + $qtd_cr + $qtd_cnh + $qtd_doc;

?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="theme-color" content="#000">
  <title>Gestão | Ventura</title>

  <!-- Custom fonts for this template-->
  <link href="css/toast.css" rel="stylesheet" type="text/css">
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
  <meta name="mobile-web-app-capable" content="yes">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">
  <link href="img/logo_lateral.png" rel="shortcut icon">

  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.8.5/css/selectize.default.css">
  <style>
    .zoom:hover {
      transform: scale(1.03);
    }

    body {
      font-size: 0.85rem;
    }

    .status {
      padding: 3px;
      color: #fff;
    }

    table.dataTable tbody th,
    table.dataTable tbody td {
      vertical-align: middle;
    }

    .zoom:hover {
      transform: scale(1.03);
    }

    .card-hover:hover {
      background: #eeefff;
    }

    .ui-autocomplete-input {
      z-index: 1511;
      position: relative;
    }

    .ui-menu .ui-menu-item a {
      font-size: 12px;
    }

    .ui-autocomplete {
      position: absolute;
      top: 0;
      left: 0;
      z-index: 1510 !important;
      float: left;
      display: none;
      list-style: none;
      -webkit-border-radius: 2px;
      -moz-border-radius: 2px;
      border-radius: 2px;
      -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
      -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
      box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
      -webkit-background-clip: padding-box;
      -moz-background-clip: padding;
      background-clip: padding-box;
      *border-right-width: 2px;
      *border-bottom-width: 2px;
    }

    .ui-menu-item>a.ui-corner-all {
      display: block;
      padding: 3px 15px;
      clear: both;
      font-weight: normal;
      line-height: 18px;
      color: #555555;
      white-space: nowrap;
      text-decoration: none;
    }

    .ui-state-hover,
    .ui-state-active {
      color: #ffffff;
      text-decoration: none;
      background-color: #0088cc;
      border-radius: 0px;
      -webkit-border-radius: 0px;
      -moz-border-radius: 0px;
      background-image: none;
    }

    table.dataTable {
      border-collapse: collapse !important;
    }

    .collapse-item {
      cursor: pointer;
    }

    .nav-item {
      cursor: pointer;
    }

    table.dataTable tbody tr {
      background-image: linear-gradient(180deg, #5a5c69 10%, #505155 100%);
    }

    label {
      color: white;
    }

    .dataTables_wrapper .dataTables_length,
    .dataTables_wrapper .dataTables_filter,
    .dataTables_wrapper .dataTables_info,
    .dataTables_wrapper .dataTables_processing,
    .dataTables_wrapper .dataTables_paginate {
      color: white;
    }

    .spinner-border {
      width: 3rem;
      height: 3rem;
    }

    .div-alertas {
      overflow-y: scroll;
      height: auto;
      max-height: 500px;
    }
  </style>
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-dark sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
        <div class="sidebar-brand-icon">
          <img id="logo-seco" src="img/logo_seco.png" style="padding: 13px;width: 80px;display:none" />
        </div>
        <div class="sidebar-brand-text mx-3"><img src="img/logo_lateral.png" style="padding: 0px;width: 180px;height: 85px;" /></div>
      </a>

      <!-- Divider -->

      <!-- Divider 
      <hr class="sidebar-divider">-->

      <!-- Nav Item - Pages Collapse Menu -->
      <?php
      if (in_array("1", $telas)) {
      ?>
      <li class="nav-item active">
        <a class="nav-link collapsed" href="#dashboard" onclick="page('dashboard')">
          <i class="fa fa-tachometer-alt text-white"></i>
          <span>Dashboard</span>
        </a>
      </li>

      <?php
      }

      if (in_array("2", $telas)) {
      ?>

      <li class="nav-item active">
        <a class="nav-link collapsed" href="#viagem" onclick="page('viagem')">
          <i class="fas fa-globe-americas text-white"></i>
          <span>Viagens</span>
        </a>
      </li>

      <?php
      }

      if (in_array("3", $telas)|| in_array("4", $telas) || in_array("5", $telas) || in_array("6", $telas) || in_array("8", $telas) 
      || in_array("9", $telas)) {
      ?>
      <hr class="sidebar-divider">
      <div class="sidebar-heading">
        Gestão
      </div>
      <?php } 
      
      if (in_array("3", $telas)) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseCaixa" aria-expanded="true" aria-controls="collapseCaixa">
          <i class="fa fa-money-bill  text-white"></i>
          <span>&nbspCaixa</span>
        </a>
        <div id="collapseCaixa" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Caixa:</h6>
            <a class="collapse-item" data-toggle="modal" data-target="#RetiraSalario">Retirada de Dinherio</a>
            <a class="collapse-item" href="#retirada" onclick="page('retirada')">Listagem de Retirada</a>
            <a class="collapse-item" href="#historico_caixa" onclick="page('historico_caixa')">Histórico do Caixa</a>
          </div>
        </div>
      </li>
      <?php } 
      
      if (in_array("4", $telas)) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseCaminhao" aria-expanded="true" aria-controls="collapseCaminhao">
          <i class="fa fa-truck text-white"></i>
          <span>&nbspCaminhões</span>
        </a>
        <div id="collapseCaminhao" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Caminhão:</h6>
            <a class="collapse-item" data-toggle="modal" data-target="#AddCaminhao">Cadastro de Caminhão</a>
            <a class="collapse-item" href="#caminhao" onclick="page('caminhao')">Listagem de Caminhões</a>
          </div>
        </div>
      </li>
      <?php
      }

      if (in_array("5", $telas)) {
      ?>

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseDestino" aria-expanded="true" aria-controls="collapseDestino">
          <i class="fa fa-truck text-white"></i>
          <span>&nbspDestinos</span>
        </a>
        <div id="collapseDestino" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Destino:</h6>
            <a class="collapse-item" data-toggle="modal" data-target="#AddDestino">Cadastro de Destino</a>
            <a class="collapse-item" href="#destino" onclick="page('destino')">Listagem de Destinos</a>
          </div>
        </div>
      </li>
      <?php
      }

      if (in_array("6", $telas)) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseFunc" aria-expanded="true" aria-controls="collapseFunc">
          <i class="fas fa-address-card text-white"></i>
          <span>&nbspFuncionários</span>
        </a>
        <div id="collapseFunc" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Funcionario:</h6>
            <a class="collapse-item" data-toggle="modal" data-target="#AddFunc">Cadastro Funcionario</a>
            <a class="collapse-item" href="#funcionario" onclick="page('funcionario')">Listagem de Funcionario</a>
          </div>
        </div>
      </li>
      <?php
      }

      if (in_array("7", $telas)) {
      ?>

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseCliente" aria-expanded="true" aria-controls="collapseCliente">
          <i class="fa fa-users text-white"></i>
          <span>&nbspClientes</span>
        </a>
        <div id="collapseCliente" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Cliente:</h6>
            <a class="collapse-item" onclick="geraClienteIndex()">Cadastro Cliente</a>
            <a class="collapse-item" href="#cliente" onclick="page('cliente')">Listagem de Clientes</a>
          </div>
        </div>
      </li>

      <?php
      }

      if (in_array("8", $telas)) {
      ?>

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseFornecedor" aria-expanded="true" aria-controls="collapseFornecedor">
          <i class="fa fa-user text-white"></i>
          <span>&nbsp&nbspFornecedores</span>
        </a>
        <div id="collapseFornecedor" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Fornecedor:</h6>
            <a class="collapse-item" data-toggle="modal" data-target="#AddFor">Cadastro Fornecedor</a>
            <a class="collapse-item" href="#fornecedor" onclick="page('fornecedor')">Listagem de Fornecedores</a>
          </div>
        </div>
      </li>

      <?php
      }

      if (in_array("9", $telas)) {
      ?>

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseBanco" aria-expanded="true" aria-controls="collapseBanco">
          <i class="fas fa-money-check-alt text-white"></i>
          <span>&nbspBancos</span>
        </a>
        <div id="collapseBanco" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Bancos:</h6>
            <a class="collapse-item" data-toggle="modal" data-target="#AddBanco">Cadastro Banco</a>
            <a class="collapse-item" href="#banco" onclick="page('banco')">Consulta Bancos</a>
          </div>
        </div>
      </li>

      <?php
      }

      if (in_array("10", $telas)) {
      ?>

      <hr class="sidebar-divider">
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseRelatorio" aria-expanded="true" aria-controls="collapseRelatorio">
          <i class="fas fa-clipboard text-white"></i>
          <span>&nbsp&nbspRelatórios</span>
        </a>
        <div id="collapseRelatorio" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Relatório:</h6>
            <a class="collapse-item" href="#relatorio_viagem" onclick="page('relatorio_viagem')">Rel. de Viagens</a>
            <a class="collapse-item" href="#relatorio_combustivel" onclick="page('relatorio_combustivel')">Rel. de Combustível</a>
            <a class="collapse-item" href="#relatorio_pedagio" onclick="page('relatorio_pedagio')">Rel. de Pedágios</a>
            <a class="collapse-item" href="#relatorio_frete" onclick="page('relatorio_frete')">Rel. de Frete</a>
          </div>
        </div>
      </li>
      <?php
      }

      if (in_array("11", $telas)||in_array("12", $telas)) {
      ?>
      <hr class="sidebar-divider">
      <div class="sidebar-heading">
        Financeiro
      </div>
      <?php } 
      
      if (in_array("11",$telas)){
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#contas_receber" onclick="page('contas_receber')">
          <i class="fa fa-barcode text-white"></i>
          <span>&nbspContas à Receber</span>
        </a>
      </li>
      <?php } 
      
      if (in_array("12",$telas)){
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#contas_pagar" onclick="page('contas_pagar')">
          <i class="fa fa-barcode text-white"></i>
          <span>&nbspContas à Pagar</span>
        </a>
      </li>
      <?php } ?>


      <hr class="sidebar-divider">

      <div class="sidebar-heading">
        Conta
      </div>


      <li class="nav-item">
        <a class="nav-link collapsed" data-toggle="collapse" data-target="#collapseUsuario" aria-expanded="true" aria-controls="collapseUsuario">
          <i class="fa fa-users text-white"></i>
          <span>&nbspUsuarios</span>
        </a>
        <div id="collapseUsuario" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Usuario:</h6>
            <a class="collapse-item" onclick="page('minha_conta')">Minha Conta</a>
            <?php if($usuario_id == 1){ ?>
            <a class="collapse-item" data-toggle="modal" data-target="#AddUser">Cadastro de Usuário</a>
            <?php }
            
            if(in_array("13",$telas)){
            ?>
            <a class="collapse-item" href="#usuario" onclick="page('usuario')">Listagem dos Usuarios</a>
            <?php } ?>
          </div>
        </div>
      </li>


    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link  rounded-circle mr-3 " onclick="logo()">
            <i class="fa fa-bars text-white"></i>
          </button>


          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">


          </ul>
          <div style="width: 100%;text-align-last: end;"><button data-toggle="modal" data-target="#AddAnotacao" class="btn btn-primary">Anotar</button></div>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Alerts -->
            <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-bell fa-fw"></i>
                <?php if ($qtd_notificacoes > 0) { ?>
                  <span class="badge badge-danger badge-counter">+<?= $qtd_notificacoes ?></span>
                <?php } else { ?>
                <?php } ?>
              </a>
              <!-- Dropdown - Alerts -->
              <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                <h6 class="dropdown-header">
                  Alertas
                </h6>
                <div class="div-alertas">
                  <?php if ($qtd_notificacoes == 0) { ?>
                    <a class="dropdown-item d-flex align-items-center" href="#">
                      <div>
                        <span class="font-weight-bold">Nenhuma notificação</span>
                      </div>
                    </a>
                  <?php } ?>
                  <?php while ($row = mysqli_fetch_array($res_cr)) { ?>
                    <a class="dropdown-item d-flex align-items-center" href="#contas_receber" onclick="page('contas_receber')">
                      <div class="mr-3">
                        <div class="icon-circle bg-danger text-white">
                          <i class="fas fa-coins"></i>
                        </div>
                      </div>
                      <div>
                        <div class="small text-gray-500"><?= date('d/m/Y', strtotime($row['vencimento'])) ?></div>
                        <span class="font-weight-bold">Conta a receber, valor de R$<?= number_format($row['valor_parcela'], 2, ',', '.') ?> vencida.</span>
                      </div>
                    </a>
                  <?php } ?>
                  <?php while ($row = mysqli_fetch_array($res_cp)) { ?>
                    <a class="dropdown-item d-flex align-items-center" href="#contas_pagar" onclick="page('contas_pagar')">
                      <div class="mr-3">
                        <div class="icon-circle bg-danger">
                          <i class="fas fa-donate text-white"></i>
                        </div>
                      </div>
                      <div>
                        <div class="small text-gray-500"><?= date('d/m/Y', strtotime($row['vencimento'])) ?></div>
                        <span class="font-weight-bold">Conta a pagar, valor de R$<?= number_format($row['valor'], 2, ',', '.') ?></span>
                      </div>
                    </a>
                  <?php } ?>
                  <?php while ($row = mysqli_fetch_array($res_cnh)) { ?>
                    <a class="dropdown-item d-flex align-items-center" href="#funcionario" onclick="page('funcionario')">
                      <div class="mr-3">
                        <div class="icon-circle bg-danger">
                          <i class="fas fa-address-card text-white"></i>
                        </div>
                      </div>
                      <div>
                        <div class="small text-gray-500"><?= date('d/m/Y', strtotime($row['vencimento_cnh'])) ?></div>
                        <span class="font-weight-bold">CNH de <?= $row['nome'] ?> vencida.</span>
                      </div>
                    </a>
                  <?php } ?>
                  <?php while ($row = mysqli_fetch_array($res_doc)) { ?>
                    <a class="dropdown-item d-flex align-items-center" href="#veiculo" onclick="page('veiculo')">
                      <div class="mr-3">
                        <div class="icon-circle bg-danger">
                          <i class="fas fa-file-alt text-white"></i>
                        </div>
                      </div>
                      <div>
                        <div class="small text-gray-500"><?= date('d/m/Y', strtotime($row['vencimento_doc'])) ?></div>
                        <span class="font-weight-bold">Documento do veículo <?= $row['modelo'] . " (" . $row['cor'] . "), placa " . $row['placa'] ?> vencido.</span>
                      </div>
                    </a>
                  <?php } ?>
                </div>

                <!-- Nav Item - Messages
            <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-envelope fa-fw"></i> -->
                <!-- Counter - Messages -->
                <!-- <span class="badge badge-danger badge-counter">7</span>
              </a> -->
                <!-- Dropdown - Messages -->
                <!-- <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">
                <h6 class="dropdown-header">
                  Message Center
                </h6>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/fn_BT9fwg_E/60x60" alt="">
                    <div class="status-indicator bg-success"></div>
                  </div>
                  <div class="font-weight-bold">
                    <div class="text-truncate">Hi there! I am wondering if you can help me with a problem I've been having.</div>
                    <div class="small text-gray-500">Emily Fowler · 58m</div>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/AU4VPcFN4LE/60x60" alt="">
                    <div class="status-indicator"></div>
                  </div>
                  <div>
                    <div class="text-truncate">I have the photos that you ordered last month, how would you like them sent to you?</div>
                    <div class="small text-gray-500">Jae Chun · 1d</div>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/CS2uCrpNzJY/60x60" alt="">
                    <div class="status-indicator bg-warning"></div>
                  </div>
                  <div>
                    <div class="text-truncate">Last month's report looks great, I am very happy with the progress so far, keep up the good work!</div>
                    <div class="small text-gray-500">Morgan Alvarez · 2d</div>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/Mv9hjnEUHR4/60x60" alt="">
                    <div class="status-indicator bg-success"></div>
                  </div>
                  <div>
                    <div class="text-truncate">Am I a good boy? The reason I ask is because someone told me that people say this to all dogs, even if they aren't good...</div>
                    <div class="small text-gray-500">Chicken the Dog · 2w</div>
                  </div>
                </a>
                <a class="dropdown-item text-center small text-gray-500" href="#">Read More Messages</a>
              </div>
            </li>
            <div class="topbar-divider d-none d-sm-block"></div> -->

                <!-- Nav Item - User Information -->


            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-white "><?php echo utf8_encode($nome); ?></span>
                <img class="rounded-circle" style="width: 60px" src="<?php echo $image; ?>" />
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <!--  <a class="dropdown-item" href="#">
                  <i class="fas fa-key fa-sm fa-fw mr-2 text-gray-400"></i>
                  Alterar Senha
                </a> -->
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>
          </ul>
        </nav>
        <!-- End of Topbar -->


        <div id="conteudo">



        </div>



        <!-- End of Content Wrapper -->
        <!-- Footer -->
        <footer class="sticky-footer">
          <div class="container my-auto">
            <div class="copyright text-center my-auto">
              <span>Copyright &copy; EvolutionSoft <?php echo date('Y'); ?></span>
            </div>
          </div>
        </footer>
        <!-- End of Footer -->
      </div>
      <!-- End of Page Wrapper -->

      <!-- Scroll to Top Button-->
      <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
      </a>

      <!-- Logout Modal-->
      <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Logout</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">Você realmente deseja sair ?</div>
            <div class="modal-footer">
              <button class="btn btn-secondary" type="button" data-dismiss="modal">Não</button>
              <a class="btn btn-primary" href="php/logout.php">Sim</a>
            </div>
          </div>
        </div>
      </div>


      <?php
      if (isset($_SESSION['msg'])) {
        $n = 1;
      ?>
        <div id="snackbar">
          <?php
          echo $_SESSION['msg'];
          unset($_SESSION['msg']);
          ?>
        </div>
      <?php }  ?>





      <!-- Bootstrap core JavaScript-->

      <script src="vendor/jquery/jquery.min.js"></script>
      <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

      <!-- Core plugin JavaScript-->
      <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

      <!-- Custom scripts for all pages-->
      <script src="js/sb-admin-2.min.js"></script>

      <!-- Page level plugins -->
      <script src="vendor/chart.js/Chart.min.js"></script>

      <!-- Page level custom scripts 
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>  -->
      <!--<script src="//code.jquery.com/jquery-1.12.4.js"></script>-->
      <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
      <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.8.5/js/standalone/selectize.min.js"></script>

</body>

<!-- MODAL DE CADASTRO  -->

<?php include_once('views/modals/cad_cliente.php'); ?>

<?php include_once('views/modals/cad_anotacao.php'); ?>

<?php include_once('views/modals/cad_fornecedor.php'); ?>

<?php include_once('views/modals/cad_funcionario.php'); ?>

<?php include_once('views/modals/cad_caminhao.php'); ?>

<?php include_once('views/modals/cad_destino.php'); ?>

<?php include_once('views/modals/cad_sangria.php'); ?>

<?php include_once('views/modals/cad_banco.php'); ?>

<?php include_once('views/modals/cad_usuario.php'); ?>


<!-- FIM DO MODAL DE CADASTRO  -->
<script>
  var l = 1;
  var url = window.location.href;
  var id = url.substring(url.lastIndexOf('#') + 1);
  var data = "<div id='spinner' class='spinner-border' role='status' style='margin-left: 47%;margin-top: 20%;margin-bottom: 20%'><span class='sr-only'>Loading...</span></div>";
  $("#conteudo").html(data);
  if (id.length > 0) {

    if (id.length == 43 || id.length == 44) {
      $(document).ready(function() {
        $('#conteudo').load("views/"+<?=$tela_inicial?>+".php");
      });
    } else {
      $(document).ready(function() {
        $('#conteudo').load("views/" + id + ".php");
      });
    }

  } else {
    $(document).ready(function() {
      $('#conteudo').load("views/"+<?=$tela_inicial?>+".php");
    });
  }

  var t = <?php echo $n; ?>;
  if (t > 0) {
    myFunction();
  }

  function myFunction() {
    // Get the snackbar DIV
    var x = document.getElementById("snackbar");

    // Add the "show" class to DIV
    x.className = "show";

    // After 3 seconds, remove the show class from DIV
    setTimeout(function() {
      x.className = x.className.replace("show", "");
    }, 3000);
  }

  function page(pagina) {
    var data = "<div id='spinner' class='spinner-border' role='status' style='margin-left: 47%;margin-top: 20%;margin-bottom: 20%'><span class='sr-only'>Loading...</span></div>";
    $("#conteudo").html(data);
    $(document).ready(function() {
      $('#conteudo').load("views/" + pagina + ".php");
    });
  }

  function geraClienteIndex() {
    $("#id_orcamento_cli").val(0);
    $("#razao").val("");
    $("#responsavel").val("");
    $("#AddCli").modal('show');
  }

  function logo() {
    l++;

    if (l % 2 == 0) {
      $('#logo-seco').css('display', 'block');
    } else {
      $('#logo-seco').css('display', 'none');
    }
  }
</script>



</html>